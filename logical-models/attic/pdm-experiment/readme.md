## pdm2xls
````
'********** instructies **********
'1. start powerdesigner met het model geopend.
'2. open met ctrl+shift+x het edit/run script window en laad daarin dit script
'3. run het script met f5

````

## xls2pdm

````

'********** instructies **********
'1. start powerdesigner zonder daarbij een bestand te openen.
'2. open met ctrl+shift+x het edit/run script window en laad daarin dit script
'3. definieer het excel bestand dat geimporteerd moete worden in powerdesigner
'   door het aanpassen van de parameters kun je het proces sturen.
'   Eventueel in blokjes inladen set par_... = 0, mochten de poppen aan het dansen zijn ;-)
'4. run het script met f5
````

````
'********** parameters **********

modelnaam ="02206_LGM_EIG_STI_CNV_2012_1_0_v1"
par_inputfile = "Z:\Downloads\"& modelnaam &".xlsx"



````
