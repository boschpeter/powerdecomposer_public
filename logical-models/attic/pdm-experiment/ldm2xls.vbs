'ldm2xls.vbs v20121010
'cc.wust@belastingdienst.nl
'p.bosch@belastingdienst.nl 

'********** parameters **********

par_alternating_colors            = false
par_insert_empty_lines            = false
par_sort_referencejoins_on_attribut = false
par_underline_primary_keys        = false

'********** excel functions **********

'excel rows and attributs are numbered 1, 2, 3, ...

'>>> definitions

dim excel_obj

excel_colorindex_nocolor = xlColorIndexNone
excel_colorindex_black   = 1
excel_colorindex_white   = 2
excel_colorindex_red     = 3
excel_colorindex_green   = 4
excel_colorindex_blue    = 5
excel_colorindex_yellow  = 6
excel_colorindex_magenta = 7
excel_colorindex_cyan    = 8

'>>> functions that are called by other functions

function excel_get_row(excel_row_nr)
   excel_get_row = cstr(excel_row_nr)
end function

function excel_get_rows(excel_row_nr1, excel_row_nr2)
   excel_get_rows = excel_get_row(excel_row_nr1) & ":" & excel_get_row(excel_row_nr2)
end function

function excel_get_attribut(excel_attribut_nr)
   letter1 = (excel_attribut_nr - 1) \ 26
   letter2 = ((excel_attribut_nr - 1) mod 26) + 1
   if letter1 = 0 then
      excel_get_attribut = chr(64 + letter2)
   else
      excel_get_attribut = chr(64 + letter1) & chr(64 + letter2)
   end if
end function

function excel_get_attributes(excel_attribut_nr1, excel_attribut_nr2)
   excel_get_attributes = excel_get_attribut(excel_attribut_nr1) & ":" & excel_get_attribut(excel_attribut_nr2)
end function

function excel_get_cell(excel_row_nr, excel_attribut_nr)
   excel_get_cell = excel_get_attribut(excel_attribut_nr) & excel_get_row(excel_row_nr)
end function

function excel_get_cells(excel_row_nr1, excel_attribut_nr1, excel_row_nr2, excel_attribut_nr2)
   excel_get_cells = excel_get_attribut(excel_attribut_nr1) & excel_get_row(excel_row_nr1) & ":" & excel_get_attribut(excel_attribut_nr2) & excel_get_row(excel_row_nr2)
end function

'>>> functions to set or read the value of a cell

function excel_set_cell_value(excel_row_nr, excel_attribut_nr, value)
   excel_obj.range(excel_get_cell(excel_row_nr, excel_attribut_nr)).value = value
   set excel_set_cell_value = nothing
end function

function excel_get_cell_value(excel_row_nr, excel_attribut_nr)
   excel_get_cell_value = excel_obj.range(excel_get_cell(excel_row_nr, excel_attribut_nr)).value
end function

'>>> i/o functions

function excel_new_file(visible, nr_sheets)
   set excel_obj = createobject("excel.application")
   excel_obj.visible = visible
   excel_obj.sheetsinnewworkbook = nr_sheets
   excel_obj.workbooks.add
   set excel_new_file = nothing
end function

function excel_load_file(visible, filename)
   set excel_obj = createobject("excel.application")
   excel_obj.visible = visible
   excel_obj.workbooks.open(filename)
   excel_obj.worksheets(1).select
   set excel_load_file = nothing
end function

function excel_save_file()
   excel_obj.activeworkbook.save
   set excel_save_file = nothing
end function

function excel_save_as_file(filename)
   excel_obj.activeworkbook.saveas(filename)
   set excel_save_as_file = nothing
end function

function excel_close_file()
   excel_obj.activeworkbook.close
   set excel_close_file = nothing
end function

'>>> worksheet functions

function excel_select_worksheet(worksheet_nr)
   excel_obj.worksheets(worksheet_nr).select
   set excel_select_worksheet = nothing
end function

function excel_set_worksheet_name(worksheet_nr, worksheet_name)
   excel_obj.worksheets(worksheet_nr).name = worksheet_name
   set excel_set_worksheet_name = nothing
end function

function excel_get_worksheet_name(worksheet_nr)
   excel_get_worksheet_name = excel_obj.worksheets(worksheet_nr).name
end function

'>>> layout functions

function excel_set_cursor(excel_row_nr, excel_attribut_nr)
   excel_obj.range(excel_get_cell(excel_row_nr, excel_attribut_nr)).select
   set excel_set_cursor = nothing
end function

function excel_merge_cells(excel_row_nr1, excel_attribut_nr1, excel_row_nr2, excel_attribut_nr2)
   excel_obj.range(excel_get_cells(excel_row_nr1, excel_attribut_nr1, excel_row_nr2, excel_attribut_nr2)).select
   excel_obj.selection.merge
   set excel_merge_cells = nothing
end function

function excel_set_zoom(zoom_percentage)
   excel_obj.activewindow.zoom = zoom_percentage
   set excel_set_zoom = nothing
end function

function excel_autofit_attributs(excel_attribut_nr1, excel_attribut_nr2)
   excel_obj.attributs(excel_get_attributs(excel_attribut_nr1, excel_attribut_nr2)).autofit
   set excel_autofit_attributs = nothing
end function

function excel_freeze_row(excel_row_nr)
   excel_obj.rows(excel_get_rows(excel_row_nr, excel_row_nr)).select
   excel_obj.activewindow.freezepanes = true
   set excel_freeze_row = nothing
end function

function excel_freeze_attribut(excel_attribut_nr)
   excel_obj.attributs(excel_get_attributs(excel_attribut_nr, excel_attribut_nr)).select
   excel_obj.activewindow.freezepanes = true
   set excel_freeze_attribut = nothing
end function

function excel_freeze_cell(excel_row_nr, excel_attribut_nr)
   excel_obj.range(excel_get_cell(excel_row_nr, excel_attribut_nr)).select
   excel_obj.activewindow.freezepanes = true
   set excel_freeze_cell = nothing
end function

function excel_set_cells_background_color(excel_row_nr1, excel_attribut_nr1, excel_row_nr2, excel_attribut_nr2, excel_colorindex)
   excel_obj.range(excel_get_cells(excel_row_nr1, excel_attribut_nr1, excel_row_nr2, excel_attribut_nr2)).interior.colorindex = excel_colorindex
   set excel_set_cells_background_color = nothing
end function
'********** matrix functions **********

'for storing data, we use a matrix with a number of rows and a number of attributs given by nr_matrix_rows and nr_matrix_attributs, respectively
'the matrix rows are numbered 0, 1, 2, ..., nr_matrix_rows - 1
'the matrix attributs are numbered 0, 1, 2, ..., nr_matrix_attributs - 1

'the matrix has three hidden attributs that are used for storing layout information
'attribut nr_matrix_attributs + 0 stores a value that is used for sorting the rows in ascending order
'attribut nr_matrix_attributs + 1 stores a colorindex that is used for displaying the row
'attribut nr_matrix_attributs + 2 stores a boolean that indicates whether or not the row must be displayed underlined

dim matrix()

function matrix_sort_ascending(matrix, nr_matrix_rows, nr_matrix_attributs)
   dim temp()
   redim temp(nr_matrix_attributs + 3)
   for i = 0 to nr_matrix_rows - 2
      for j = i + 1 to nr_matrix_rows - 1
         if matrix(i, nr_matrix_attributs + 0) > matrix(j, nr_matrix_attributs + 0) then
            for matrix_attribut_nr = 0 to nr_matrix_attributs + 2
               temp(matrix_attribut_nr)      = matrix(i, matrix_attribut_nr)
               matrix(i, matrix_attribut_nr) = matrix(j, matrix_attribut_nr)
               matrix(j, matrix_attribut_nr) = temp(matrix_attribut_nr)
            next
         end if
      next
   next
   set matrix_sort_ascending = nothing
end function

function matrix_write_to_excel(matrix, nr_matrix_rows, nr_matrix_attribut, excel_start_row_nr, excel_start_attribut_nr)
   for matrix_row_nr = 0 to nr_matrix_rows - 1
      if matrix_row_nr = 0 then excel_row_nr = excel_start_row_nr else excel_row_nr = excel_row_nr + 1
      'if (par_insert_empty_lines and matrix_row_nr > 0) then
         'if matrix(matrix_row_nr - 1, nr_matrix_attributs + 1) <> matrix(matrix_row_nr, nr_matrix_attributs + 1) then excel_row_nr = excel_row_nr + 1
      'end if
      for matrix_attribut_nr = 0 to nr_matrix_attributs - 1
         excel_attribut_nr = excel_start_attribut_nr + matrix_attribut_nr
         set dummy = excel_set_cell_value(excel_row_nr, excel_attribut_nr, matrix(matrix_row_nr, matrix_attribut_nr))
      next
      'if par_alternating_colors then
         'set dummy = excel_set_cells_font_color(excel_row_nr, excel_start_attribut_nr, excel_row_nr, excel_start_attribut_nr + nr_matrix_attributs - 1, matrix(matrix_row_nr, nr_matrix_attributs + 1))
      'else
         'set dummy = excel_set_cells_font_color(excel_row_nr, excel_start_attribut_nr, excel_row_nr, excel_start_attribut_nr + nr_matrix_attributs - 1, excel_colorindex_1)
      'end if
      'if par_underline_primary_keys then
         'set dummy = excel_set_cells_underline(excel_row_nr, excel_start_attribut_nr, excel_row_nr, excel_start_attribut_nr + nr_matrix_attributs - 1, matrix(matrix_row_nr, nr_matrix_attributs + 2))
      'end if
   next
   set matrix_write_to_excel = nothing
end function

'********** main program **********

main

private sub main

   '>>> definitions

   excel_colorindex_header = excel_colorindex_yellow  'header color
   excel_colorindex_1      = excel_colorindex_black   'color 1
   excel_colorindex_2      = excel_colorindex_blue    'color 2

   '>>> get the pdm filename and version

   pdm_filename = activemodel.filename
   do while instr(pdm_filename, "\") > 0
      pdm_filename = right(pdm_filename, len(pdm_filename) - instr(pdm_filename, "\"))
   loop
   pdm_version = activemodel.version

   '>>> create a new excel file

   set dummy = excel_new_file(false, 6)

   '>>> sheet 1: table

   'initialize the matrix
   nr_matrix_rows = 0
   for each table in activemodel.entities
      if not table.isshortcut then
         nr_matrix_rows = nr_matrix_rows + 1
      end if
   next
   nr_matrix_attributs = 5
   redim matrix(nr_matrix_rows, nr_matrix_attributs + 3)

   'fill the matrix and sort it
   matrix_row_nr = 0
   for each table in activemodel.entities
      if not table.isshortcut then
         matrix(matrix_row_nr, 0) = table.code
         matrix(matrix_row_nr, 1) = table.name
         matrix(matrix_row_nr, 2) = table.stereotype
         matrix(matrix_row_nr, 3) = table.comment
         matrix(matrix_row_nr, 4) = table.annotation
         matrix(matrix_row_nr, nr_matrix_attributs + 0) = matrix(matrix_row_nr, 0)
         matrix(matrix_row_nr, nr_matrix_attributs + 2) = false
         matrix_row_nr = matrix_row_nr + 1
      end if
   next
   set dummy = matrix_sort_ascending(matrix, nr_matrix_rows, nr_matrix_attributs)
   for matrix_row_nr = 0 to nr_matrix_rows - 1
      if matrix_row_nr mod 2 = 0 then
         matrix(matrix_row_nr, nr_matrix_attributs + 1) = excel_colorindex_1
      else
         matrix(matrix_row_nr, nr_matrix_attributs + 1) = excel_colorindex_2
      end if
   next

   'write the matrix to excel
   set dummy = excel_select_worksheet(1)
   set dummy = excel_set_worksheet_name(1, "entities")
   set dummy = excel_set_cell_value(1, 1, "pdm_filename")
   set dummy = excel_set_cell_value(1, 2, "pdm_version")
   set dummy = excel_set_cell_value(2, 1, "entities.code")
   set dummy = excel_set_cell_value(2, 2, "entities.name")
   set dummy = excel_set_cell_value(2, 3, "entities.stereotype")
   set dummy = excel_set_cell_value(2, 4, "entities.comment")
   set dummy = excel_set_cell_value(2, 5, "entities.annotation")
   'set dummy = excel_set_cells_bold(1, 1, 1, 2, true)
   'set dummy = excel_set_cells_background_color(1, 1, 2, nr_matrix_attributs, excel_colorindex_header)
   'set dummy = matrix_write_to_excel(matrix, nr_matrix_rows, nr_matrix_attributs, 3, 1)
   'set dummy = excel_freeze_row(3)
   'set dummy = excel_autofit_attributes(1, nr_matrix_attributes)
   'set dummy = excel_set_cursor(1, 1)

   '>>> sheet 2: attribut

   'initialize the matrix
   nr_matrix_rows = 0
   for each entities in activemodel.entities
      if not entities.isshortcut then
         for each attribut in entities.attributes
            nr_matrix_rows = nr_matrix_rows + 1
         next
      end if
   next
   nr_matrix_attributes = 11
   redim matrix(nr_matrix_rows, nr_matrix_attributes + 3)

   'fill the matrix and sort it
   matrix_row_nr = 0
   for each entities in activemodel.entities
         if not entities.isshortcut then
         for each attribut in entities.attributes
            matrix(matrix_row_nr, 0) = entities.code
            matrix(matrix_row_nr, 1) = attribut.code
            matrix(matrix_row_nr, 2) = attribut.name
            matrix(matrix_row_nr, 3) = attribut.format
            matrix(matrix_row_nr, 4) = ucase(attribut.datatype)
            
            'if attribut.primary then matrix(matrix_row_nr, 5)   = "X"
            'if attribut.mandatory then matrix(matrix_row_nr, 6) = "X"
'           	if attribut.indicator then matrix(matrix_row_nr, 8)   = "X"
            matrix(matrix_row_nr, 8) = attribut.stereotype
            matrix(matrix_row_nr, 9) = attribut.comment
            matrix(matrix_row_nr, 10) = attribut.annotation
            matrix(matrix_row_nr, nr_matrix_attributes + 0) = matrix(matrix_row_nr, 0) &  "_" & (1000000 + matrix_row_nr)
            'matrix(matrix_row_nr, nr_matrix_attributes + 2) = attribut.primary
            matrix_row_nr = matrix_row_nr + 1
         next
      end if
   next
   set dummy = matrix_sort_ascending(matrix, nr_matrix_rows, nr_matrix_attributs)
   for matrix_row_nr = 0 to nr_matrix_rows - 1
      if matrix_row_nr = 0 then
         matrix(matrix_row_nr, nr_matrix_attributs + 1) = excel_colorindex_1
      else
         if matrix(matrix_row_nr - 1, 0) <> matrix(matrix_row_nr, 0) then
            if matrix(matrix_row_nr - 1, nr_matrix_attributs + 1) = excel_colorindex_2 then
               matrix(matrix_row_nr, nr_matrix_attributs + 1) = excel_colorindex_1
            else
               matrix(matrix_row_nr, nr_matrix_attributs + 1) = excel_colorindex_2
            end if
         else
            matrix(matrix_row_nr, nr_matrix_attributs + 1) = matrix(matrix_row_nr - 1, nr_matrix_attributs + 1)
         end if
      end if
   next

   'write the matrix to excel
   set dummy = excel_select_worksheet(2)
   set dummy = excel_set_worksheet_name(2, "attributes")
   set dummy = excel_set_cell_value(1, 1, pdm_filename)
   set dummy = excel_set_cell_value(1, 2, pdm_version)
   set dummy = excel_set_cell_value(2, 1, "table.code")
   set dummy = excel_set_cell_value(2, 2, "attribut.code")
   set dummy = excel_set_cell_value(2, 3, "attribut.name")
   set dummy = excel_set_cell_value(2, 4, "attribut.format")
   set dummy = excel_set_cell_value(2, 5, "attribut.datatype")
'   set dummy = excel_set_cell_value(2, 6, "attribut.identity")
   set dummy = excel_set_cell_value(2, 6, "attribut.primary")
   set dummy = excel_set_cell_value(2, 7, "attribut.indicator")
   set dummy = excel_set_cell_value(2, 8, "attribut.mandatory")
   set dummy = excel_set_cell_value(2, 9, "attribut.stereotype")
   set dummy = excel_set_cell_value(2, 10, "attribut.comment")
   set dummy = excel_set_cell_value(2, 11, "attribut.annotation")
   'set dummy = excel_set_cells_bold(1, 1, 1, 2, true)
   'set dummy = excel_set_cells_background_color(1, 1, 2, nr_matrix_attributs, excel_colorindex_header)
   'set dummy = matrix_write_to_excel(matrix, nr_matrix_rows, nr_matrix_attributs, 3, 1)
   'set dummy = excel_freeze_row(3)
   'set dummy = excel_autofit_attributs(1, nr_matrix_attributs)
   set dummy = excel_set_cursor(1, 1)

   '>>> sheet 3: reference

   'initialize the matrix
   nr_matrix_rows = 0
   'for each reference in activemodel.references
      'if not reference.isshortcut then
      'nr_matrix_rows = nr_matrix_rows + 1
      'end if
   'next
   nr_matrix_attributs = 8
   redim matrix(nr_matrix_rows, nr_matrix_attributs + 3)

   'fill the matrix and sort it
   matrix_row_nr = 0
   set dummy = matrix_sort_ascending(matrix, nr_matrix_rows, nr_matrix_attributs)
   for matrix_row_nr = 0 to nr_matrix_rows - 1
      if matrix_row_nr mod 2 = 0 then
         matrix(matrix_row_nr, nr_matrix_attributs + 1) = excel_colorindex_1
      else
         matrix(matrix_row_nr, nr_matrix_attributs + 1) = excel_colorindex_2
      end if
   next

   'write the matrix to excel
   set dummy = excel_select_worksheet(3)
   set dummy = excel_set_worksheet_name(3, "Relationships")
   set dummy = excel_set_cell_value(1, 1, pdm_filename)
   set dummy = excel_set_cell_value(1, 2, pdm_version)
   set dummy = excel_set_cell_value(2, 1, "reference.code")
   set dummy = excel_set_cell_value(2, 2, "reference.name")
   set dummy = excel_set_cell_value(2, 3, "reference.childtable.code")
   set dummy = excel_set_cell_value(2, 4, "reference.parenttable.code")
   set dummy = excel_set_cell_value(2, 5, "reference.cardinality")
   set dummy = excel_set_cell_value(2, 6, "reference.stereotype")
   set dummy = excel_set_cell_value(2, 7, "reference.comment")
   set dummy = excel_set_cell_value(2, 8, "reference.annotation")
   'set dummy = excel_set_cells_bold(1, 1, 1, 2, true)
   'set dummy = excel_set_cells_background_color(1, 1, 2, nr_matrix_attributs, excel_colorindex_header)
   'set dummy = matrix_write_to_excel(matrix, nr_matrix_rows, nr_matrix_attributs, 3, 1)
   'set dummy = excel_freeze_row(3)
   'set dummy = excel_autofit_attributs(1, nr_matrix_attributs)
   'set dummy = excel_set_cursor(1, 1)


   '>>> sheet 4: join

   'initialize the matrix
   nr_matrix_rows = 0
   nr_matrix_attributs = 5
   redim matrix(nr_matrix_rows, nr_matrix_attributs + 3)

   'fill the matrix and sort it
   matrix_row_nr = 0
   
   set dummy = matrix_sort_ascending(matrix, nr_matrix_rows, nr_matrix_attributs)
   for matrix_row_nr = 0 to nr_matrix_rows - 1
      if matrix_row_nr = 0 then
         matrix(matrix_row_nr, nr_matrix_attributs + 1) = excel_colorindex_1
      else
         if (matrix(matrix_row_nr - 1, 1) & "_" & matrix(matrix_row_nr - 1, 3)) <> (matrix(matrix_row_nr, 1) & "_" & matrix(matrix_row_nr , 3)) then
            if matrix(matrix_row_nr - 1, nr_matrix_attributs + 1) = excel_colorindex_2 then
               matrix(matrix_row_nr, nr_matrix_attributs + 1) = excel_colorindex_1
            else
               matrix(matrix_row_nr, nr_matrix_attributs + 1) = excel_colorindex_2
            end if
         else
            matrix(matrix_row_nr, nr_matrix_attributs + 1) = matrix(matrix_row_nr - 1, nr_matrix_attributs + 1)
         end if
      end if
   next

   'write the matrix to excel
   set dummy = excel_select_worksheet(4)
   set dummy = excel_set_worksheet_name(4, "joins")
   set dummy = excel_set_cell_value(1, 1, pdm_filename)
   set dummy = excel_set_cell_value(1, 2, pdm_version)
   set dummy = excel_set_cell_value(2, 1, "reference.code")
   set dummy = excel_set_cell_value(2, 2, "reference.childtable.code")
   set dummy = excel_set_cell_value(2, 3, "join.childtableattribut.code")
   set dummy = excel_set_cell_value(2, 4, "reference.parenttable.code")
   set dummy = excel_set_cell_value(2, 5, "join.parenttableattribut.code")
   'set dummy = excel_set_cells_bold(1, 1, 1, 2, true)
    'set dummy = excel_set_cells_background_color(1, 1, 2, nr_matrix_attributs, excel_colorindex_header)
    '  set dummy = matrix_write_to_excel(matrix, nr_matrix_rows, nr_matrix_attributs, 3, 1)
   'set dummy = excel_freeze_row(3)
   'set dummy = excel_autofit_attributs(1, nr_matrix_attributs)
   'set dummy = excel_set_cursor(1, 1)

   '>>> sheet 5: diagram

   'initialize the matrix
   nr_matrix_rows = 0
   for each diagram in activemodel.logicaldiagrams
      nr_matrix_rows = nr_matrix_rows + 1
   next
   nr_matrix_attributs = 4
   redim matrix(nr_matrix_rows, nr_matrix_attributs + 3)

   'fill the matrix and sort it
   matrix_row_nr = 0
   for each diagram in activemodel.logicaldiagrams
      matrix(matrix_row_nr, 0) = diagram.code
      matrix(matrix_row_nr, 1) = diagram.name
      matrix(matrix_row_nr, 2) = diagram.pageformat
      matrix(matrix_row_nr, 3) = diagram.pageorientation
      matrix(matrix_row_nr, nr_matrix_attributs + 0) = matrix(matrix_row_nr, 0)
      matrix(matrix_row_nr, nr_matrix_attributs + 2) = false
      matrix_row_nr = matrix_row_nr + 1
   next
   set dummy = matrix_sort_ascending(matrix, nr_matrix_rows, nr_matrix_attributs)
   for matrix_row_nr = 0 to nr_matrix_rows - 1
      if matrix_row_nr mod 2 = 0 then
         matrix(matrix_row_nr, nr_matrix_attributs + 1) = excel_colorindex_1
      else
         matrix(matrix_row_nr, nr_matrix_attributs + 1) = excel_colorindex_2
      end if
   next

   'write the matrix to excel
   set dummy = excel_select_worksheet(5)
   set dummy = excel_set_worksheet_name(5, "diagrams")
   set dummy = excel_set_cell_value(1, 1, pdm_filename)
   set dummy = excel_set_cell_value(1, 2, pdm_version)
   set dummy = excel_set_cell_value(2, 1, "diagram.code")
   set dummy = excel_set_cell_value(2, 2, "diagram.name")
   set dummy = excel_set_cell_value(2, 3, "diagram.pageformat")
   set dummy = excel_set_cell_value(2, 4, "diagram.pageorientation")
   'set dummy = excel_set_cells_bold(1, 1, 1, 2, true)
   'set dummy = excel_set_cells_background_color(1, 1, 2, nr_matrix_attributs, excel_colorindex_header)
   'set dummy = matrix_write_to_excel(matrix, nr_matrix_rows, nr_matrix_attributs, 3, 1)
   'set dummy = excel_freeze_row(3)
   'set dummy = excel_autofit_attributs(1, nr_matrix_attributs)
   'set dummy = excel_set_cursor(1, 1)

   '>>> sheet 6: symbol

   'initialize the matrix
   nr_matrix_rows = 0
   for each diagram in activemodel.logicaldiagrams
      for each symbol in diagram.symbols
         if ((symbol.objecttype = "EntitiesSymbol") or (symbol.objecttype = "ReferenceSymbol")) then
            nr_matrix_rows = nr_matrix_rows + 1
         end if
      next
   next
   nr_matrix_attributs = 11
   redim matrix(nr_matrix_rows, nr_matrix_attributs + 3)

   'fill the matrix and sort it
   matrix_row_nr = 0
   for each diagram in activemodel.logicaldiagrams
      for each symbol in diagram.symbols
         if ((symbol.objecttype = "EntitiesSymbol") or (symbol.objecttype = "ReferenceSymbol")) then
            matrix(matrix_row_nr, 0) = diagram.code
            matrix(matrix_row_nr, 1) = symbol.objecttype
            matrix(matrix_row_nr, 2) = symbol.code
            matrix(matrix_row_nr, 3) = symbol.name
            matrix(matrix_row_nr, 4) = symbol.rect.top
            matrix(matrix_row_nr, 5) = symbol.rect.bottom
            matrix(matrix_row_nr, 6) = symbol.rect.left
            matrix(matrix_row_nr, 7) = symbol.rect.right
            matrix(matrix_row_nr, 8) = symbol.linecolor
            if symbol.objecttype = "EntitieSymbol" then
               matrix(matrix_row_nr, 9) = symbol.fillcolor
               matrix(matrix_row_nr, 10) = symbol.gradientfillmode
            end if
            matrix(matrix_row_nr, nr_matrix_attributes + 0) = matrix(matrix_row_nr, 0) & "_" & (matrix(matrix_row_nr, 1) = "ReferenceSymbol") & "_" & matrix(matrix_row_nr, 2)
            matrix(matrix_row_nr, nr_matrix_attributes + 2) = false
            matrix_row_nr = matrix_row_nr + 1
         end if
      next
   next
   set dummy = matrix_sort_ascending(matrix, nr_matrix_rows, nr_matrix_attributes)
   for matrix_row_nr = 0 to nr_matrix_rows - 1
      if matrix_row_nr = 0 then
         matrix(matrix_row_nr, nr_matrix_attributes + 1) = excel_colorindex_1
      else
         if matrix(matrix_row_nr, 0) <> matrix(matrix_row_nr - 1, 0) then
            if matrix(matrix_row_nr - 1, nr_matrix_attributs + 1) = excel_colorindex_2 then
               matrix(matrix_row_nr, nr_matrix_attributs + 1) = excel_colorindex_1
            else
               matrix(matrix_row_nr, nr_matrix_attributes + 1) = excel_colorindex_2
            end if
         else
            matrix(matrix_row_nr, nr_matrix_attributes + 1) = matrix(matrix_row_nr - 1, nr_matrix_attributes + 1)
         end if
      end if
   next

   'write the matrix to excel
   set dummy = excel_select_worksheet(6)
   set dummy = excel_set_worksheet_name(6, "symbols")
   set dummy = excel_set_cell_value(1, 1, pdm_filename)
   set dummy = excel_set_cell_value(1, 2, pdm_version)
   set dummy = excel_set_cell_value(2, 1, "diagram.code")
   set dummy = excel_set_cell_value(2, 2, "symbol.objecttype")
   set dummy = excel_set_cell_value(2, 3, "symbol.code")
   set dummy = excel_set_cell_value(2, 4, "symbol.name")
   set dummy = excel_set_cell_value(2, 5, "symbol.rect.top")
   set dummy = excel_set_cell_value(2, 6, "symbol.rect.bottom")
   set dummy = excel_set_cell_value(2, 7, "symbol.rect.left")
   set dummy = excel_set_cell_value(2, 8, "symbol.rect.right")
   set dummy = excel_set_cell_value(2, 9, "symbol.linecolor")
   set dummy = excel_set_cell_value(2, 10, "symbol.fillcolor")
   set dummy = excel_set_cell_value(2, 11, "symbol.gradientfillmode")
   'set dummy = excel_set_cells_bold(1, 1, 1, 2, true)
   'set dummy = excel_set_cells_background_color(1, 1, 2, nr_matrix_attributes, excel_colorindex_header)
   'set dummy = matrix_write_to_excel(matrix, nr_matrix_rows, nr_matrix_attributes, 3, 1)
   'set dummy = excel_freeze_row(3)
   'set dummy = excel_autofit_attributs(1, nr_matrix_attributes)
   'set dummy = excel_set_cursor(1, 1)

   '>>> close the excel file

   set dummy = excel_select_worksheet(1)
   set dummy = excel_save_as_file(left(activemodel.filename, len(activemodel.filename) - 4) & "_cm.xlsx")
   set dummy = excel_close_file()

end sub 
