'******************************************************************************
'* File:            :xls2pdm.vbs
'* version          :20171103 V19
'* File Type        :VB Script File (needs Powerdesigner plus excel environment installed) exec {CTRL+SHIFT+x}
'* Purpose          :import Logical Model from Excel into PDM model     
'* Title:           :xls2pdm
'* Category         :BI&D ontwerp script
'* Identificatie    :\BI&D_EDW_MTHV\1130 Hulpmiddelen\3000 Scripts\3200 Ontwerp 

'Versiebeheer
'18 uitbreiding van attributenoverzicht met ABO tag (in powerdesigner: column.ClientCheckExpression) 
'19 uitbreiding van model.annotation met v_EDW_Bronsyteem en v_EDW_CDW_view. Tevens Rtf2Ascii toegevoegd[ESSEN03]

'big thanks to cc.wust@belastingdienst.nl  
'hello to nj.essenstam@belastingdienst.nl 
'p.bosch@belastingdienst.nl
'r.michel@belastingdienst.nl

'*rationale
'*De verwerking van een bronmodel naar een doelmodel is een tijdrovend en minutieus proces.
'*Daarbij mogen geen fouten worden gemaakt. Een oude wijsheid uit de systeemontwikkeling zegt dat fouten 
'*in het voortraject achteraf veel meer tijd kosten om te corrigeren dan fouten achter in het traject.
'*De ontwerpers van het EDW beschikken hiermee over een wizard die ondersteunt in hun werkzaamheden:
'*repetitief werk wordt verricht door tooling, terwijl ontwerkeuzes via stuurparameters door de ontwerpers aan 
'*de tooling wordt meegegeven. Hiermee bestaat een goede balans tussen geautomatiseerde en handmatig ontwerp.

'********** instructies **********
'1. start powerdesigner zonder daarbij een bestand te openen.
'2. open met ctrl+shift+x het edit/run script window en laad daarin dit script
'3. definieer het excel bestand dat geimporteerd moete worden in powerdesigner
'   door het aanpassen van de parameters kun je het proces sturen.
'   Eventueel in blokjes inladen set par_... = 0, mochten de poppen aan het dansen zijn ;-)
'4. run het script met f5

'********** parameters **********

'modelnaam ="01400_LGM_EDW_CDP"
modelnaam ="05218_LGM_BAS_STI_BRV"
par_inputfile = "N:\BAS\BRV\Ontwerp\20171103\"& modelnaam &".xlsx"

'** Parameter en versiebeheer
par_titel           = true

'**datalaag
par_entities        = true
par_attributes      = true
par_relations       = true
par_joins           = true

'**presentatielaag  
par_diagrams        = true
par_symbols         = true
lettertype          = "Arial,9"


'par_views           = true   
'par_view_columns    = true
'par_ViewReferences  = true
'par_vwref_joins     = true

' optionele check parameters
verwacht_aantal_entities   = 0
verwacht_aantal_attributes = 0
verwacht_aantal_relations  = 0
verwacht_aantal_joins      = 0
verwacht_aantal_diagrams   = 0
verwacht_aantal_symbols    = 0
verwacht_aantal_views      = 0

'------------------------------------------------------------------------------     
vbstartdate = date                                                                  
vbstarttime = time                                                                  

'------------------------------------------------------------------------------     
'main function  The programms starts here actually                                  
'------------------------------------------------------------------------------     

main                                                                                

'------------------------------------------------------------------------------     
'main function  The programms ends here actually                                  
'------------------------------------------------------------------------------     

private sub main                                                                    
	
'>>> if there is no model, create a new one
	if activemodel is nothing then
		set model = createmodel(pdpdm.cls_model, "|DBMS=IBM DB2 Version 10 for z//OS")
		model.name = modelnaam
		model.code = ucase(model.name)
		default_diagram = true
		
		dim dgmDoelDiagram 'object the diagram represents
		set dgmDoelDiagram = activemodel.physicalDiagrams.item(0) 
		dgmDoelDiagram.code= "tt" 
		dgmDoelDiagram.name= "tt"
	else
		default_diagram = false
	end if
	
'>>> open the excel file
	set dummy = excel_load_file(false, par_inputfile)
	
	set dummy = excel_unhide_worksheets 
	set dummy = excel_unhide_columns
	
'********************
	If par_titel then
		
'parameters
		v_EDW_Domein           = excel_get_cell_value(7,2 )
		v_EDW_Recordbron       = excel_get_cell_value(8,2 )
		v_EDW_Volgnummer       = excel_get_cell_value(9,2 )
		v_EDW_Type_ontsl       = excel_get_cell_value(10,2 )
		v_EDW_Prefix           = excel_get_cell_value(11,2 )
		v_EDW_Bronsyteem       = excel_get_cell_value(12,2 )
      v_EDW_CDW_view         = excel_get_cell_value(13,2 )

		output "v_EDW_Domein = " & v_EDW_Domein
		output "v_EDW_Recordbron= " & v_EDW_Recordbron
		output "v_EDW_Volgnummer= "& v_EDW_Volgnummer
		output "v_EDW_Type_ontsl= " & v_EDW_Type_ontsl
		output "v_EDW_Prefix= " & v_EDW_Prefix
      output "v_EDW_Bronsyteem= " & v_EDW_Bronsyteem
      output "v_EDW_CDW_view= " & v_EDW_CDW_view
		output "  "
		
		comment1="PARAMETERS" 
		comment2="(Structuur , nummering en : niet aanpassen !!)"
		comment3=" "
		comment4="1.EDW Domein" & Chr(9) & Chr(9) & ": " & v_EDW_Domein 
		comment5="2.EDW Recordbronnaam" & Chr(9) & ": " & v_EDW_Recordbron
		comment6="3.EDW Volgnummer" & Chr(9) & ": " & v_EDW_Volgnummer 
		comment7="4.EDW Type ontsluiting" & Chr(9) & ": " & v_EDW_Type_ontsl
		comment8="5.EDW Prefix" & Chr(9) & Chr(9) & ": " & v_EDW_Prefix
      comment9="6.EDW Bronsysteem" & Chr(9) & ": " & v_EDW_Bronsyteem
      comment10="7.EDW CDW View" & Chr(9) & ": " & v_EDW_CDW_VIEW
      activemodel.annotation=Rtf2Ascii(comment1 &Chr(10)& comment2 &Chr(10)& comment3 &Chr(10)& comment4 &Chr(10)& comment5 &Chr(10)& comment6 & Chr(10)& comment7 &Chr(10)& comment8 &Chr(10)& comment9 &Chr(10)& comment10)
      
'versiebeheer
		comment1="VERSIEBEHEER"	
		comment2="(Structuur , nummering en | niet aanpassen !!)"
		comment3=" "			
		comment4="Versie"& Chr(9) & "|Basisversie"& Chr(9) &"|datum" & Chr(9) & Chr(9)& "|Auteur" & Chr(9) & Chr(9)& "|Taak                           " & Chr(9) & Chr(9) & Chr(9)& Chr(9)  &"|Toelichting" & Chr(9)
		v_description=Rtf2Ascii(comment1 & Chr(10) & comment2 & Chr(10) & comment3 & Chr(10) & comment4)
		
'author en version 
		v_auth = excel_get_cell_value(100,1)
		v_vers = excel_get_cell_value(100,2)
		activemodel.author  = v_auth
		activemodel.version = v_vers
		
' description overzetten (releases) 
		i=20
		do while i < 70
			v_versie = trim(excel_get_cell_value(i,1))
			v_basisversie = trim(excel_get_cell_value(i,2))
			v_datum=	trim(excel_get_cell_value(i,3))
			v_auteur= excel_get_cell_value(i,4)
			v_taak=	excel_get_cell_value(i,5)
			v_toelichting= excel_get_cell_value(i,6)
    
    		if v_versie <> "'" then v_versie=v_versie & space(15-len(v_versie)) 
			if v_basisversie <> "'" then v_basisversie=v_basisversie & space(20-len(v_basisversie)) 
			if v_datum <> "'" then v_datum=v_datum & space(20-len(v_datum)) 
        'if v_auteur <> "'" then v_auteur=v_auteur & space(20-len(v_auteur)) 
			
			If trim(v_versie) <> "" then v_description=v_description & Chr(10) &  v_versie & "|"& v_basisversie & Chr(9) & "|"& v_Datum & Chr(9) &"|"& v_auteur & Chr(9)& "|"& v_taak & Chr(9)  &"|"& v_toelichting
			If trim(v_versie) = "" then i = 70
			i=i+1
		Loop 
		activemodel.description= Rtf2Ascii(v_description)
		
' Comment overzetten (Wijzigingshistorie)
		j=101
		do while j < 1101
			v_comm = trim(excel_get_cell_value(j,1))
			
			v_comment=v_comment & v_comm 
			
			If trim(v_comm) = "" then j = 1101
			j=j+1
		Loop 
		activemodel.comment= Rtf2Ascii(v_comment)
		
	end if
	
'********************
	if par_entities then  
'>>> sheet 2: entities
		for each table in activemodel.tables
			if not table.isshortcut then
				table.description = "X"  
			end if
		next
		set dummy = excel_select_worksheet(2)      
		excel_row_nr = 3
'activemodel.version= excel_get_cell_value(excel_row_nr, 1)  'set version
		
		prev_line_empty = false
		curr_line_empty = (excel_get_cell_value(excel_row_nr, 5) = "")  '5 is name table
		while not (prev_line_empty and curr_line_empty)
			if (not curr_line_empty) then
				
'getter       
				table_annotation1           = excel_get_cell_value(excel_row_nr,1 )        'I                               table.annotation          
				table_annotation2           = excel_get_cell_value(excel_row_nr,2 )        'U                               table.annotation          
				table_annotation3           = excel_get_cell_value(excel_row_nr,3 )        'D                               table.annotation          
				table_beginscript           = excel_get_cell_value(excel_row_nr,4 )        'USE-CASE                        table.beginscript         
				table_name                  = excel_get_cell_value(excel_row_nr,5 )        'FILE                            table.name                
				table_CheckConstraintName   = excel_get_cell_value(excel_row_nr,6 )        'META-DATA INDICATOR             table.CheckConstraintName 
				table_description           = excel_get_cell_value(excel_row_nr,7 )        'CALCULATION / TRANSFORMATION    table.description         
				table_endscript             = excel_get_cell_value(excel_row_nr,8 )        'ENVIRONMENT                     table.endscript           
				table_code                  = excel_get_cell_value(excel_row_nr,9 )        'ENTITY                          table.code                
				table_stereotype            = excel_get_cell_value(excel_row_nr,10)        'ENTITY STEREOTYPE               table.stereotype          
				table_type                  = excel_get_cell_value(excel_row_nr,11)        'CALCULATION / TRANSFORMATION    table.type                
				table_comment               = excel_get_cell_value(excel_row_nr,12)        'ENTITY REMARKS                  table.comment             
				table_character             = excel_get_cell_value(excel_row_nr,13)        'CHARACTERSET                    table.CheckExpressionPreview
				
				table_index = get_table_index(table_code)
				if table_index = -1 then
					output excel_row_nr & "> add table " & table_code
					table_annotation =  table_annotation1&":>>"&table_annotation2&":>>"&table_annotation3
					
'setter
					set table = activemodel.tables.createnew()
					with table
						.annotation            = table_annotation            ' I U D                        table.annotation           1 
''.annotation            = table_annotation          ' U                            table.annotation           2 
''.annotation            = table_annotation          ' D                            table.annotation           3 
						.beginscript           = table_beginscript          ' USE-CASE                      table.beginscript          4 
						.name                  = table_name                 ' FILE                          table.name                 5 
						.CheckConstraintName   = table_CheckConstraintName  ' META-DATA INDICATOR           table.CheckConstraintName  6 
						.description           = table_description          ' CALCULATION / TRANSFORMATION  table.description          7 
						.endscript             = table_endscript            ' ENVIRONMENT                   table.endscript            8 
						.code                  = table_code                 ' ENTITY                        table.code                 9 
						.stereotype            = table_stereotype           ' ENTITY STEREOTYPE             table.stereotype           10
						.type                  = table_type                 ' CALCULATION / TRANSFORMATION  table.type                 11
						.comment               = table_comment              ' ENTITY REMARKS                table.comment              12
						.CheckExpressionPreview= table_character            ' CHARACTERSET                  table.CheckExpressionPreview13
						
					end with              
				else
					set table = activemodel.tables.item(table_index)
					table.description = ""
					if table.name <> table_name then
						output "> update table " & table_code & ": name = " & table.name & " -> name = " & table_name
						table.name = table_name
					end if
					if table.stereotype <> table_stereotype then
						output "> update table " & table_code & ": stereotype = " & table.stereotype & " -> stereotype = " & table_stereotype
						table.stereotype = table_stereotype
					end if
					if table.comment <> table_comment then
						output "> update table " & table_code & ": comment = " & table.comment & " -> comment = " & table_comment
						table.comment = table_comment
					end if
					if table.annotation <> table_annotation then
						output "> update table " & table_code & ": annotation = " & table.annotation & " -> annotation = " & table_annotation
						table.annotation = table_annotation
					end if
				end if
			end if
			excel_row_nr = excel_row_nr + 1
			prev_line_empty = curr_line_empty
			curr_line_empty = (excel_get_cell_value(excel_row_nr, 5) = "")   '5 is nametable     
		wend
		
		for each table in activemodel.tables
			if not table.isshortcut then
				if table.description = "X" then
					output "> delete table " & table.code
					table.delete()
				end if
			end if
		Next                                                                                  
	end if ' par_entities
	
'******************** 
	if par_attributes  then  
'>>> sheet 3: attributes
		
		for each table in activemodel.tables
			if not table.isshortcut then
				for each column in table.columns
					column.description = "X" 
				next
			end if
		next
		
		set dummy = excel_select_worksheet(3)
		excel_row_nr = 3
		prev_line_empty = false
		curr_line_empty = (excel_get_cell_value(excel_row_nr,5) = "") 'kolom5 filename
		while not (prev_line_empty and curr_line_empty)
			if (not curr_line_empty) then
				
'getter 	
				column_annotation1        = excel_get_cell_value(excel_row_nr,1 )   ' I                                1       column.annotation     
				column_annotation2        = excel_get_cell_value(excel_row_nr,2 )   ' U                                2       column.annotation     
				column_annotation3        = excel_get_cell_value(excel_row_nr,3 )   ' D                                3       column.annotation     
				table_name                = excel_get_cell_value(excel_row_nr,4 )   ' FILE                             4       table.name            
				column_name               = excel_get_cell_value(excel_row_nr,5 )   ' FIELD                            5       column.name           
				column_format             = excel_get_cell_value(excel_row_nr,6 )   ' FIELD-DATATYPE                   6       column.format         
				column_physicaloptions    = excel_get_cell_value(excel_row_nr,7 )   ' FIELD-SEQUENCE                   7       column.physicaloptions
				column_lowvalue           = excel_get_cell_value(excel_row_nr,8 )   ' FIELD-STARTPOSITION              8       column.lowvalue       
				column_highvalue          = excel_get_cell_value(excel_row_nr,9 )   ' FIELD-ENDPOSITION                9       column.highvalue      
				column_unit               = excel_get_cell_value(excel_row_nr,10)   ' FIELD-LENGTH                     10      column.unit           
				column_primary            = excel_get_cell_value(excel_row_nr,11)   ' PK/AK                            11      column.primary        
				column_mandatory          = excel_get_cell_value(excel_row_nr,12)   ' MANDATORY                        12      column.mandatory      
				column_nospace            = excel_get_cell_value(excel_row_nr,13)   ' PK NULLABLE                      13      column.nospace        
				column_description        = excel_get_cell_value(excel_row_nr,14)   ' CALCULATION / TRANSFORMATION     14      column.description    
				table_endscript           = excel_get_cell_value(excel_row_nr,15)   ' ENVIRONMENT                      15      table.endscript       
				table_code                = excel_get_cell_value(excel_row_nr,16)   ' ENTITY                           16      table.code            
				column_code               = excel_get_cell_value(excel_row_nr,17)   ' ATTRIBUTE                        17      column.code           
				column_datatype           = excel_get_cell_value(excel_row_nr,18)   ' DATA TYPE                        18      column.datatype       
				column_stereotype         = excel_get_cell_value(excel_row_nr,19)   ' ATTRIBUTE STEREOTYPE             19      column.stereotype     
				column_PhysicalOptions    = excel_get_cell_value(excel_row_nr,20)   ' CALCULATION / TRANSFORMATION     20      column.ComputedExpression
'column_primary            = excel_get_cell_value(excel_row_nr,21)  ' PK/AK                            21      column.primary        
'column_mandatory          = excel_get_cell_value(excel_row_nr,22)  ' MANDATORY                        22      column.mandatory      
'column_nospace            = excel_get_cell_value(excel_row_nr,23)  ' PK NULLABLE                      23      column.nospace        
				column_ClientCheckExpression = excel_get_cell_value(excel_row_nr,24)   ' ABO TAGs                24      column.ClientCheckExpression        
				column_comment            = excel_get_cell_value(excel_row_nr,25)   ' ATTRIBUTE REMARKS       25      column.comment        
				
				table_index = get_table_index(table_code)
				if table_index = -1 then
					output "> error in worksheet " & excel_get_worksheet_name(2) & ", row "& excel_row_nr & ": table " & table_code & " is undefined"
				else
					set table = activemodel.tables.item(table_index)
					column_index = get_column_index(table_index, column_code)
					if column_index = -1 then
						output excel_row_nr & "> add column " & table_code & "." & column_code
						column_annotation =  column_annotation1&":>>"&column_annotation2&":>>"&column_annotation3  
'setter 
						set column = table.columns.createnew()
						with column
							.annotation               = column_annotation         '   I  U D                            1     column.annotation     
'' .annotation               = column_annotation         '   U                               2     column.annotation     
'' .annotation               = column_annotation         '   D                               3     column.annotation     
''table.name               = table_name                '   FILE                              4     table.name            
							.name                     = column_name               '   FIELD                             5     column.name           
							.format                   = column_format             '   FIELD-DATATYPE                    6     column.format         
							.physicaloptions          = column_physicaloptions    '   FIELD-SEQUENCE                    7     column.physicaloptions
							.lowvalue                 = column_lowvalue           '   FIELD-STARTPOSITION               8     column.lowvalue       
							.highvalue                = column_highvalue          '   FIELD-ENDPOSITION                 9     column.highvalue      
							.unit                     = column_unit               '   FIELD-LENGTH                      10    column.unit   
'translations        
							if column_mandatory = "X"   then  .mandatory = true  end if          'NULLABLE              12 
							if column_primary   = "PK"  then  .primary  = true   end if          'PK/AK                 11
							if column_primary   = "AK"  then  set_businesskey (column)   end if  'AK                    11
							if column_nospace   = "X"   then  .nospace   = true  end if           'PK NULLABLE          13  
'/translations 
							
							.description              = column_description        '   CALCULATION / TRANSFORMATION      14    column.description    
''table.endscript         = table_endscript           '   ENVIRONMENT                       15    table.endscript       
''table.code              = table_code                '   ENTITY                            16    table.code            
							.code                     = column_code               '   ATTRIBUTE                         17    column.code           
							.datatype                 = column_datatype           '   DATA TYPE                         18    column.datatype       
							.stereotype               = column_stereotype         '   ATTRIBUTE STEREOTYPE              19    column.stereotype     
							.ComputedExpression       = column_ComputedExpression '   CALCULATION / TRANSFORMATION      20    column.computedExpression
				                        .ClientCheckExpression    = column_ClientCheckExpression '   ABO TAGs                 24    column.ClientCheckExpression 
                                                        .comment                  = column_comment            '   ATTRIBUTE REMARKS                 25    column.comment 
                  						end with   
						
					else
						set column = table.columns.item(column_index)
						column.description = ""
						if column.name <> column_name then
							output "> update column " & table_code & "."& column_code & ": name = " & column.name & " -> name = " & column_name
							column.name = column_name
						end if
						if column.datatype <> column_datatype then
							output "> update column " & table_code & "."& column_code & ": datatype = " & column.datatype & " -> datatype = " & column_datatype
							column.datatype = column_datatype
						end if
						if column.mandatory <> column_mandatory then
							output "> update column " & table_code & "."& column_code & ": mandatory = " & column.mandatory & " -> mandatory = " & column_mandatory
							column.mandatory = column_mandatory
						end if
						if column.primary <> column_primary then
							output "> update column " & table_code & "."& column_code & ": primary = " & column.primary & " -> primary = " & column_primary
							column.primary = column_primary
						end if
						if column.stereotype <> column_stereotype then
							output "> update column " & table_code & "."& column_code & ": stereotype = " & column.stereotype & " -> stereotype = " & column_stereotype
							column.stereotype = column_stereotype
						end if
						if column.comment <> column_comment then
							output "> update column " & table_code & "."& column_code & ": comment = " & column.comment & " -> comment = " & column_comment
							column.comment = column_comment
						end if
						if column.annotation <> column_annotation then
							output "> update column " & table_code & "."& column_code & ": annotation = " & column.annotation & " -> annotation = " & column_annotation
							column.annotation = column_annotation
						end if
					end if
				end if
			end if
			
			excel_row_nr = excel_row_nr + 1
			prev_line_empty = curr_line_empty
			curr_line_empty = (excel_get_cell_value(excel_row_nr, 5) = "")    '  kolom5 filename
		wend 'while not (prev_line_empty and curr_line_empty)
		
		for each table in activemodel.tables
			if not table.isshortcut then
				for each column in table.columns
					if column.description = "X" then
						output "> delete column " & table.code & "." & column.code
						column.delete()
					end if
				next
			end if
		next
		
	end if 'par_attributes
	
'********************
	if par_relations then 
'>>> sheet 4: reference
' Check default setting !! via Tool model options reference
' default link on creation =  user defined  (Disable bullet primary key !)  
		
		Dim MdlOptions
		Set MdlOptions = ActiveModel.GetModelOptions()
		
		
		MdlOptions.ReferenceDefaultLinkOnCreation  = true  ' means user defined
		MdlOptions.ReferenceAutoReuseColumns = false      ' Robbert Michel 20161110
		MdlOptions.ReferenceAutomigrateColumns = false    ' Robbert Michel 20161110
		
'MdlOptions.ReferenceDefaultLinkOnCreation  = false ' means primary key
		
		for each reference in activemodel.references
			if not reference.isshortcut then
				reference.description = "X" 
			end if
		next
		set dummy = excel_select_worksheet(4)
		
		excel_row_nr = 3
		prev_line_empty = false
		curr_line_empty = (excel_get_cell_value(excel_row_nr, 4) = "")
		while not (prev_line_empty and curr_line_empty)
			if (not curr_line_empty) then
				
'getter
				reference_annotation1               = excel_get_cell_value(excel_row_nr,1 )    '   reference.annotation                1 
				reference_annotation2               = excel_get_cell_value(excel_row_nr,2 )    '   reference.annotation                2 
				reference_annotation3               = excel_get_cell_value(excel_row_nr,3 )    '   reference.annotation                3 
				reference_name                      = excel_get_cell_value(excel_row_nr,4 )    '   reference.name                      4 
				reference_childtable_name           = excel_get_cell_value(excel_row_nr,5 )    '   reference.childtable.name           5 
				reference_parenttable_name          = excel_get_cell_value(excel_row_nr,6 )    '   reference.parenttable.name          6 
				reference_cardinality               = excel_get_cell_value(excel_row_nr,7 )    '   reference.cardinality               7 
				reference_DeleteConstraint          = excel_get_cell_value(excel_row_nr,8 )    '   reference.DeleteConstraint          8 
				reference_UpdateConstraint          = excel_get_cell_value(excel_row_nr,9 )    '   reference.UpdateConstraint          9 
				reference_ChangeParentAllowed       = excel_get_cell_value(excel_row_nr,10)    '   reference.ChangeParentAllowed       10
				reference_childrole                 = excel_get_cell_value(excel_row_nr,11)    '   reference.childrole                 11
				reference_parentrole                = excel_get_cell_value(excel_row_nr,12)    '   reference.parentrole                12
				reference_comment                   = excel_get_cell_value(excel_row_nr,13)    '   reference.comment                   13
				reference_code                      = excel_get_cell_value(excel_row_nr,14)    '   reference.code                      14
				reference_childtable_code           = excel_get_cell_value(excel_row_nr,15)    '   reference.childtable.code           15
				reference_parenttable_code          = excel_get_cell_value(excel_row_nr,16)    '   reference.parenttable.code          16
				reference_stereotype                = excel_get_cell_value(excel_row_nr,17)    '   reference.stereotype                17
				reference_ImplementationType        = excel_get_cell_value(excel_row_nr,18)    '   reference.ImplementationType        18
				reference_description               = excel_get_cell_value(excel_row_nr,19)    '   reference.description               19
				reference_JoinExpression            = excel_get_cell_value(excel_row_nr,20)    '   reference.JoinExpression            20
				reference_ForeignKeyColumnList      = excel_get_cell_value(excel_row_nr,21)    '   reference.ForeignKeyColumnList      21
				reference_ParentKeyColumnList       = excel_get_cell_value(excel_row_nr,22)    '   reference.ParentKeyColumnList       22
				reference_foreignkeyconstraintname  = excel_get_cell_value(excel_row_nr,23)    '   reference.foreignkeyconstraintname  23
				
				
				
				reference_childtable_index = get_table_index(reference_childtable_code)
				if reference_childtable_index = -1 then
					output "> error in worksheet " & excel_get_worksheet_name(3) & ", row "& excel_row_nr & ": table " & reference_childtable_code & " is undefined"
				else
					set reference_childtable = activemodel.tables.item(reference_childtable_index)
				end if
				reference_parenttable_index = get_table_index(reference_parenttable_code)
				if reference_parenttable_index = -1 then
					output "> error in worksheet " & excel_get_worksheet_name(3) & ", row "& excel_row_nr & ": table " & reference_parenttable_code & " is undefined"
				else
					set reference_parenttable = activemodel.tables.item(reference_parenttable_index)
				end if
				reference_index = get_reference_index(reference_code)
				if reference_index = -1 then
					output excel_row_nr & "> add reference " & reference_code
					
					childtabnr  = get_table_index(reference_childtable_code )
					parenttabnr = get_table_index(reference_parenttable_code )
					
					set childobj = activemodel.tables.item(childtabnr)  '--- assumes this is child             
					set parentobj = activemodel.tables.item(parenttabnr) '--- assumes this is parent             
					
					reference_annotation =  reference_annotation1&":>>"&reference_annotation2&":>>"&reference_annotation3  
					
'setter
					set newref = activemodel.References.CreateNew()                                          
					with newref   
						.annotation                     =     reference_annotation                      '   1     reference.annotation              
'' .annotation                   =     reference_annotation                      '   2     reference.annotation              
'' .annotation                   =     reference_annotation                      '   3     reference.annotation              
						.name                           =     reference_name                            '   4     reference.name                    
''   .childtable.name              =     reference_childtable_name                 '   5     reference.childtable.name         
''   .parenttable.name             =     reference_parenttable_name                '   6     reference.parenttable.name        
						.cardinality                    =     reference_cardinality                     '   7     reference.cardinality        
'translation      
						if reference_DeleteConstraint =  "X"  then  .DeleteConstraint = "1"  end if        ' reference.DeleteConstraint           8      0 1 2 3 of 4
						if reference_updateConstraint =  "X"  then  .updateConstraint = "1" end if         ' reference.UpdateConstraint           9      0 1 2 3 of 4  
						if reference_ChangeParentAllowed =  "X" then .ChangeParentAllowed = true  end if   ' reference.ChangeParentAllowed        10     boolean
'/ einde translatrion
						
						.childrole                      =     reference_childrole                       '   11     reference.childrole               
						.parentrole                     =     reference_parentrole                      '   12     reference.parentrole              
						.comment                        =     reference_comment                         '   13    reference.comment                 
						.code                         =     reference_code                            '   14    reference.code                    
''   .childtable.code              =     reference_childtable_code                 '   15    reference.childtable.code         
						.ChildTable  = childobj    
''   .parenttable.code             =     reference_parenttable_code                '   16    reference.parenttable.code  
						.ParentTable = parentobj         
						.stereotype                     =     reference_stereotype                      '   17    reference.stereotype              
						if reference_ImplementationType  = "X"  then .ImplementationType = "T"  end if     '  18    reference.DeleteConstraint   T= maak een SAT tabel onder de LNK kassabon D=niets
						.description                    =     reference_description                     '   19    reference.description             
''   .JoinExpression               =     reference_JoinExpression                  '   20    reference.JoinExpression          
''   .ForeignKeyColumnList         =     reference_ForeignKeyColumnList            '   21    reference.ForeignKeyColumnList    
''   .ParentKeyColumnList          =     reference_ParentKeyColumnList             '   22    reference.ParentKeyColumnList     
''   .foreignkeyconstraintname     =     reference_foreignkeyconstraintname        '   23    reference.foreignkeyconstraintname
						.foreignkeyconstraintname = "FK_" & cstr(childtabnr) & "_" & cstr(parenttabnr)    
'.foreignkeyconstraintname = reference_code  
						
					end with  
					
				else
					set reference = activemodel.references.item(reference_index)
					reference.description = ""
					if reference.name <> reference_name then
						output "> update reference " & reference_code & ": name = " & reference.name & " -> name = " & reference_name
						reference.name = reference_name
					end if
					if reference.childtable.code <> reference_childtable.code then
						output "> update reference " & reference_code & ": childtable = " & reference.childtable.code & " -> childtable = " & reference_childtable.code
						set reference.childtable = reference_childtable
					end if
					if reference.parenttable.code <> reference_parenttable.code then
						output "> update reference " & reference_code & ": parenttable = " & reference.parenttable.code & " -> parenttable = " & reference_parenttable.code
						set reference.parenttable = reference_parenttable
					end if
					if reference.stereotype <> reference_stereotype then
						output "> update reference " & reference_code & ": stereotype = " & reference.stereotype & " -> stereotype = " & reference_stereotype
						reference.stereotype = reference_stereotype
					end if
					if reference.comment <> reference_comment then
						output "> update reference " & reference_code & ": comment = " & reference.comment & " -> comment = " & reference_comment
						reference.comment = reference_comment
					end if
					if reference.annotation <> reference_annotation then
						output "> update reference " & reference_code & ": annotation = " & reference.annotation & " -> annotation = " & reference_annotation
						reference.annotation = reference_annotation
					end if
				end if
			end if
			excel_row_nr = excel_row_nr + 1
			prev_line_empty = curr_line_empty
			curr_line_empty = (excel_get_cell_value(excel_row_nr, 4) = "")      
		wend
		for each reference in activemodel.references
			if not reference.isshortcut then
				if reference.description = "X" then
					output "> delete reference " & reference.code
					reference.delete()
				end if
			end if
		next
		
	end if 'par_relations 
	
	
	
'********************
	if par_joins then 
'>>> sheet 5: joins 
' Check default setting on create reference !! via Tool->model->options->reference
' default link on creation =  user defined  (Disable bullet primary key !)  
' MdlOptions.ReferenceDefaultLinkOnCreation  = true  ' means user defined , dus zoals het in het koppelvlak staat
''''>< MdlOptions.ReferenceDefaultLinkOnCreation  = false ' means primary key  dan maakt pwddes automatisch kolommen aan
		
		for each reference in activemodel.references
			reference.description = "" 
		next
		
		set dummy = excel_select_worksheet(5)
		
		excel_row_nr = 3
		prev_line_empty = false
		curr_line_empty = (excel_get_cell_value(excel_row_nr, 1) = "")
		
		while not (prev_line_empty and curr_line_empty)
			if (not curr_line_empty) then
				
'getter 
				reference_name                        = excel_get_cell_value(excel_row_nr,1 ) '   reference.name                1 
				reference_childtable_name             = excel_get_cell_value(excel_row_nr,2 ) '   reference.childtable.name     2 
				join_childtablecolumn_name            = excel_get_cell_value(excel_row_nr,3 ) '   join.childtablecolumn.name    3 
				reference_parenttable_name            = excel_get_cell_value(excel_row_nr,4 ) '   reference.parenttable.name    4 
				join_parenttablecolumn_name           = excel_get_cell_value(excel_row_nr,5 ) '   join.parenttablecolumn.name   5 
				reference_code                        = excel_get_cell_value(excel_row_nr,6 ) '   reference.code                6 
				reference_childtable_code             = excel_get_cell_value(excel_row_nr,7 ) '   reference.childtable.code     7 
				join_childtablecolumn_code            = excel_get_cell_value(excel_row_nr,8 ) '   join.childtablecolumn.code    8 
				reference_parenttable_code            = excel_get_cell_value(excel_row_nr,9 ) '   reference.parenttable.code    9 
				join_parenttablecolumn_code           = excel_get_cell_value(excel_row_nr,10) '   join.parenttablecolumn.code   10
				
				output excel_row_nr &"> add refjoin " & reference_childtable_code  & "." & join_childtablecolumn_code  & "->" & reference_parenttable_code & "." &  join_parenttablecolumn_code   
				
				for each reference in activemodel.references   
					if not reference.isshortcut then
						
						if reference.code = reference_code    then  
							for each parentcol in reference.ParentTable.Columns
								if  parentcol.code  =  join_parenttablecolumn_code then
									exit for
								end if
							next 'parcol
							
							for each childcol in reference.childTable.Columns
								if  childcol.code  =  join_childtablecolumn_code then
									exit for
								end if
							next 'childcol
							
'setter
							set newjoin   = reference.joins.CreateNew()  
							with newjoin
								.ParentTableColumn     =  parentcol   
								.childTableColumn      =  childcol
							end with  
						end if  '  reference.code = reference_cod
					end if   'not reference.isshortcut then
				next 'reference   
				
			end if  '(not curr_line_empty) then
			excel_row_nr = excel_row_nr + 1
			prev_line_empty = curr_line_empty
			curr_line_empty = (excel_get_cell_value(excel_row_nr, 1) = "")      
			
		wend 'while not (prev_line_empty and curr_line_empty)
	end if 'par_joins
	
'********************
	if par_views then 
'>>> sheet 6: views
		for each view in activemodel.views
			if not view.isshortcut then
				view.description = "X" 
			end if
		next
		set dummy = excel_select_worksheet(6)
		excel_row_nr = 3
		prev_line_empty = false
		curr_line_empty = (excel_get_cell_value(excel_row_nr, 4) = "")
		
		while not (prev_line_empty and curr_line_empty)
			if (not curr_line_empty) then
				
				view_annotation =  view_annotation1&":>>"&view_annotation2&":>>"&viewe_annotation3         
'getter
				view_annotation     =    excel_get_cell_value(excel_row_nr,1 ) '     view.annotation     1 
'      view_annotation     =   = excel_get_cell_value(excel_row_nr,2 ) '     view.annotation     2 
'      view_annotation     =   = excel_get_cell_value(excel_row_nr,3 ) '     view.annotation     3 
				view_code           =    excel_get_cell_value(excel_row_nr,4 ) '     view.code           4 
				view_name           =   excel_get_cell_value(excel_row_nr,5 ) '     view.name           5 
				view_stereotype     =   excel_get_cell_value(excel_row_nr,6 ) '     view.stereotype     6 
				view_comment        =    excel_get_cell_value(excel_row_nr,7 ) '     view.comment        7 
				view_type           =    excel_get_cell_value(excel_row_nr,8 ) '     view.type           8 
				view_description    =    excel_get_cell_value(excel_row_nr,9 ) '     view.description    9 
				view_sqlquery       =    excel_get_cell_value(excel_row_nr,10) '     view.sqlquery       10
				
				view_sqlquery =  replace (view_sqlquery , ":>>" ,  vbCrLf) 
				
				output excel_row_nr & "> add view " & view_code
				view_index = get_view_index(view_code)
				
				if view_index = -1 then
					set view = activemodel.views.createnew()
					with view
						.code        = view_code
						.name        = view_name
						.stereotype  = view_stereotype
						.comment     = view_comment
						.annotation  = view_annotation
						.description = view_description
						.sqlquery    = view_sqlquery
						
					end with              
				end if
			end if
			excel_row_nr = excel_row_nr + 1
			prev_line_empty = curr_line_empty
			curr_line_empty = (excel_get_cell_value(excel_row_nr, 4) = "")      
		wend  'while not (prev_line_empty and curr_line_empty) 
		
	end if 'par_views 
	
'******************  
	if par_view_columns  then  '>>> sheet 7: view_attributes
		
		set dummy = excel_select_worksheet(7)
		excel_row_nr = 3
		prev_line_empty = false
		curr_line_empty = (excel_get_cell_value(excel_row_nr,4) = "")
		sqlquery = "SELECT " & vbCrLf
		
		while not (prev_line_empty and curr_line_empty)
			
			if (not curr_line_empty) then
				view_code              = excel_get_cell_value(excel_row_nr, 4)
				next_view_code         =  excel_get_cell_value(excel_row_nr + 1 , 4)  ' +1 betekent er komt een nieuwe view  aan
				column_code            = excel_get_cell_value(excel_row_nr, 5)
				column_comment          = excel_get_cell_value(excel_row_nr, 13)
				column_format          = excel_get_cell_value(excel_row_nr, 7)    ' from 
				column_description     = excel_get_cell_value(excel_row_nr, 14)   ' where 
				column_stereotype      = excel_get_cell_value(excel_row_nr, 12)   ' and
				
				view_index = get_view_index(view_code)
				if view_index = -1 then
					output "> error in worksheet " & excel_get_worksheet_name(2) & ", row "& excel_row_nr & ": view " & view_code & " is undefined"
				else
					set view = activemodel.views.item(view_index)
					output excel_row_nr &  "> add view column " & view_code & "." & column_code
					sqlquery = sqlquery  &" " & column_comment &"."& column_code
					
					
					if  view_code = next_view_code then  
						sqlquery = sqlquery &  ","  & vbCrLf 
					end if
					
					if  view_code <> next_view_code then 
'output " laatste regel van " & view_code
						sqlquery = sqlquery  & vbCrLf  
						sqlquery = sqlquery &  column_format     & vbCrLf   ' from
						sqlquery = sqlquery & column_description  & vbCrLf  ' where 
						sqlquery = sqlquery & column_stereotype   & vbCrLf  ' and maxdat=31-12-9999 latest view
' output sqlquery
						
						if  view.sqlquery  = "" then
							view.sqlquery = sqlquery  ' ******create view columns from sqlstatement. only if nothing specified yet.
						end if 
						
						sqlquery = "SELECT " & vbCrLf ' 
					end if
				end if  '  view_index = -1 then
			end if   ' (not curr_line_empty) then 
			excel_row_nr = excel_row_nr + 1
			prev_line_empty = curr_line_empty
			curr_line_empty = (excel_get_cell_value(excel_row_nr, 4) = "")      
		wend  ' while not (prev_line_empty and curr_line_empty)
	end if 'par_viewcolumns
	
	
	
'********************
	if par_ViewReferences then 
		
		set dummy = excel_select_worksheet(17)
		
		excel_row_nr = 3
		Viewreference_teller = -1
		prev_line_empty = false
		curr_line_empty = (excel_get_cell_value(excel_row_nr, 4) = "")
		while not (prev_line_empty and curr_line_empty)
			if (not curr_line_empty) then
				Viewreference_teller = Viewreference_teller + 1
'getter
				Viewreference_annotation1               = excel_get_cell_value(excel_row_nr,1 )    '   reference.annotation                1 
				Viewreference_annotation2               = excel_get_cell_value(excel_row_nr,2 )    '   reference.annotation                2 
				Viewreference_annotation3               = excel_get_cell_value(excel_row_nr,3 )    '   reference.annotation                3 
				Viewreference_name                      = excel_get_cell_value(excel_row_nr,4 )    '   reference.name                      4 
				Viewreference_childtable_name           = excel_get_cell_value(excel_row_nr,5 )    '   reference.childtable.name           5 
				Viewreference_parenttable_name          = excel_get_cell_value(excel_row_nr,6 )    '   reference.parenttable.name          6 
				Viewreference_cardinality               = excel_get_cell_value(excel_row_nr,7 )    '   reference.cardinality               7 
				Viewreference_DeleteConstraint          = excel_get_cell_value(excel_row_nr,8 )    '   reference.DeleteConstraint          8 
				Viewreference_UpdateConstraint          = excel_get_cell_value(excel_row_nr,9 )    '   reference.UpdateConstraint          9 
				Viewreference_ChangeParentAllowed       = excel_get_cell_value(excel_row_nr,10)    '   reference.ChangeParentAllowed       10
				Viewreference_childrole                 = excel_get_cell_value(excel_row_nr,11)    '   reference.childrole                 11
				Viewreference_parentrole                = excel_get_cell_value(excel_row_nr,12)    '   reference.parentrole                12
				Viewreference_comment                   = excel_get_cell_value(excel_row_nr,13)    '   reference.comment                   13
				Viewreference_code                      = excel_get_cell_value(excel_row_nr,14)    '   reference.code                      14
				Viewreference_childview_code            = excel_get_cell_value(excel_row_nr,15)    '   reference.childtable.code           15
				Viewreference_parentview_code           = excel_get_cell_value(excel_row_nr,16)    '   reference.parenttable.code          16
				Viewreference_stereotype                = excel_get_cell_value(excel_row_nr,17)    '   reference.stereotype                17
				Viewreference_ImplementationType        = excel_get_cell_value(excel_row_nr,18)    '   reference.ImplementationType        18
				Viewreference_description               = excel_get_cell_value(excel_row_nr,19)    '   reference.description               19
				Viewreference_JoinExpression            = excel_get_cell_value(excel_row_nr,20)    '   reference.JoinExpression            20
				Viewreference_ForeignKeyColumnList      = excel_get_cell_value(excel_row_nr,21)    '   reference.ForeignKeyColumnList      21
				Viewreference_ParentKeyColumnList       = excel_get_cell_value(excel_row_nr,22)    '   reference.ParentKeyColumnList       22
				Viewreference_foreignkeyconstraintname  = excel_get_cell_value(excel_row_nr,23)    '   reference.foreignkeyconstraintname  23
				
				viewreference_childview_index = get_view_index(Viewreference_childview_code)
				if viewreference_childview_index  = -1 then
					output "> error in worksheet " & excel_get_worksheet_name(17) & ", row "& excel_row_nr & ": view " & Viewreference_childview_code   & " is undefined"
				else
					set viewreference_childview = activemodel.views.item(viewreference_childview_index)
				end if
				
				viewreference_parentview_index = get_view_index(Viewreference_parentview_code)
				if viewreference_childview_index  = -1 then
					output "> error in worksheet " & excel_get_worksheet_name(17) & ", row "& excel_row_nr & ": view " & Viewreference_parentview_code   & " is undefined"
				else
					set viewreference_parentview = activemodel.views.item(viewreference_parentview_index)
				end if
				
				Viewreference_index = get_viewreference_index(viewreference_code)
				
				if reference_index = -1 then
					output excel_row_nr & "> add viewreference " & viewreference_code
					
					childvwnr  = get_view_index(viewreference_childview_code )
					parentvwnr  = get_view_index(viewreference_parentview_code )
					
'output Viewreference_childview_code &"-"& childview_index  & "=" & Viewreference_parentview_code &"-"& parentview_index
					
					set childobj = activemodel.views.item(childvwnr)  '--- assumes this is child             
					set parentobj = activemodel.views.item(parentvwnr) '--- assumes this is parent             
					
' output childobj.code & "->" & parentobj.code
					
' reference_annotation =  reference_annotation1&":>>"&reference_annotation2&":>>"&reference_annotation3  
					
'setter
					set newref = activemodel.ViewReferences.CreateNew()                                          
					with newref   
'? .annotation                     =     reference_annotation                      '   1     reference.annotation              
'' .annotation                   =     reference_annotation                      '   2     reference.annotation              
'' .annotation                   =     reference_annotation                      '   3     reference.annotation              
						.name                           =     Viewreference_name                            '   4     reference.name                    
''   .childtable.name              =     reference_childtable_name                 '   5     reference.childtable.name         
''   .parenttable.name             =     reference_parenttable_name                '   6     reference.parenttable.name        
'?  .cardinality                    =     reference_cardinality                     '   7     reference.cardinality        
'?  .childrole                      =     reference_childrole                       '   11     reference.childrole               
'?  .parentrole                     =     reference_parentrole                      '   12     reference.parentrole              
'?   .comment                        =     reference_comment                         '   13    reference.comment                 
						.code                         =     Viewreference_code                            '   14    reference.code                    
''   .childtable.code              =     reference_childtable_code                 '   15    reference.childtable.code         
						.tableview2  = childobj    
''   .parenttable.code             =     reference_parenttable_code                '   16    reference.parenttable.code  
						.tableview1 = parentobj         
'?   .stereotype                     =     reference_stereotype                      '   17    reference.stereotype              
'?   .description                    =     reference_description                     '   19    reference.description             
''   .JoinExpression               =     reference_JoinExpression                  '   20    reference.JoinExpression          
''   .ForeignKeyColumnList         =     reference_ForeignKeyColumnList            '   21    reference.ForeignKeyColumnList    
''   .ParentKeyColumnList          =     reference_ParentKeyColumnList             '   22    reference.ParentKeyColumnList     
'?     .foreignkeyconstraintname     =     reference_foreignkeyconstraintname        '   23    reference.foreignkeyconstraintname
'.foreignkeyconstraintname = reference_code  
						
					end with  
					
				end if 'viewreference_index = -1 then
			end if
			excel_row_nr = excel_row_nr + 1
			prev_line_empty = curr_line_empty
			curr_line_empty = (excel_get_cell_value(excel_row_nr, 4) = "")      
		wend
		
'Set MdlOptions = ActiveModel.GetModelOptions()
' fix er worden automatisch attributen gekoppeld. Dat willen we hier nog niet. Userdefined.
		for each vref  in activemodel.viewreferences
			if not vref.isshortcut then
				
				for each vrefjoin  in  vref.joins
					vrefjoin.delete ()        
				next
			end if 'not reference.isshortcut t
		next  'STI.references
		
	end if 'par_ViewReferences
	
	
	
	
'********************
'begin setters van de presentatielaag.
'********************
	if par_diagrams then  
'>>> sheet 8: diagrams 
		
		set dummy = excel_select_worksheet(8)
		excel_row_nr = 3
		prev_line_empty = false
		curr_line_empty = (excel_get_cell_value(excel_row_nr, 1) = "")
		while not (prev_line_empty and curr_line_empty)
			if (not curr_line_empty) then
				
'getter
				diagram_index           = get_diagram_index(excel_get_cell_value(excel_row_nr, 1))
				diagram_code            = excel_get_cell_value(excel_row_nr, 1)
				diagram_name            = excel_get_cell_value(excel_row_nr, 2)
				diagram_pageformat      = excel_get_cell_value(excel_row_nr, 3)
				diagram_pageorientation = excel_get_cell_value(excel_row_nr, 4)
				
				if diagram_index = -1 then
					
					output excel_row_nr & "> add diagram " & diagram_code  
					diagram_teller =  diagram_teller + 1   
'setter    
					set newdiagram = activemodel.physicaldiagrams.createnew()
					with newdiagram                                                                              
						.code           = diagram_code
						.name            =  diagram_name
						.pageorientation = diagram_pageorientation
						.pageformat      = diagram_pageformat 
					end with 
				end if            
			end if ' (not curr_line_empty) then
			
			excel_row_nr = excel_row_nr + 1
			prev_line_empty = curr_line_empty
			curr_line_empty = (excel_get_cell_value(excel_row_nr, 1) = "")     
		wend  'while not (prev_line_empty and curr_line_empty)
		
	end if ' par_diagrams
'********************
	if par_symbols then
'>>> sheet 9: symbols   
		set dummy = excel_select_worksheet(9)
		excel_row_nr = 3
		prev_line_empty = false
		curr_line_empty = (excel_get_cell_value(excel_row_nr, 1) = "")
		while not (prev_line_empty and curr_line_empty)
			if (not curr_line_empty) then
' getter
				diagram_code                 =   excel_get_cell_value(excel_row_nr, 1 )       
				symbol_objecttype            =   excel_get_cell_value(excel_row_nr, 2 )       
				symbol_code                  =   excel_get_cell_value(excel_row_nr, 3 )       
				symbol_name                  =   excel_get_cell_value(excel_row_nr, 4 )       
				symbol_rect_top              =   excel_get_cell_value(excel_row_nr, 5 )        
				symbol_rect_bottom           =   excel_get_cell_value(excel_row_nr, 6 )       
				symbol_rect_left             =   excel_get_cell_value(excel_row_nr, 7 )       
				symbol_rect_right            =   excel_get_cell_value(excel_row_nr, 8 )       
				symbol_linecolor             =   excel_get_cell_value(excel_row_nr, 9 )       
				symbol_fillcolor             =   excel_get_cell_value(excel_row_nr, 10)       
				symbol_gradientfillmode      =   excel_get_cell_value(excel_row_nr, 11)       
				symbol_AutoAdjustToText      =   excel_get_cell_value(excel_row_nr, 12)       
				symbol_KeepAspect            =   excel_get_cell_value(excel_row_nr, 13)       
				symbol_KeepCenter            =   excel_get_cell_value(excel_row_nr, 14)       
				symbol_KeepSize              =   excel_get_cell_value(excel_row_nr, 15)       
				
				diagram_index = get_diagram_index(diagram_code)      
				set diagram = activemodel.physicaldiagrams.item(diagram_index)         
				
'setter table symbol           
				if symbol_objecttype = "TableSymbol" then
					table_index = get_table_index(symbol_code )
					if table_index <> -1 then
						set table = activemodel.tables.item(table_index)
' output table.code
						if diagram.findsymbol(table) is nothing then
							output excel_row_nr & "> "& diagram.code &  " add table symbol for  " &  excel_get_cell_value(excel_row_nr, 3)
							set symbol = diagram.attachobject(table)
							if symbol_rect_top         <> "" then symbol.rect.top           = symbol_rect_top        
							if symbol_rect_bottom      <> "" then symbol.rect.bottom        = symbol_rect_bottom     
							if symbol_rect_left        <> "" then symbol.rect.left          = symbol_rect_left       
							if symbol_rect_right       <> "" then symbol.rect.right         = symbol_rect_right      
							if symbol_linecolor        <> "" then symbol.linecolor          = symbol_linecolor       
							if symbol_fillcolor        <> "" then symbol.fillcolor          = symbol_fillcolor       
							if symbol_gradientfillmode <> "" then symbol.gradientfillmode   = symbol_gradientfillmode
							if symbol_AutoAdjustToText <> "" then symbol.AutoAdjustToText   = symbol_AutoAdjustToText
							if symbol_KeepAspect       <> "" then symbol.KeepAspect         = symbol_KeepAspect      
							if symbol_KeepCenter       <> "" then symbol.KeepCenter         = symbol_KeepCenter      
							if symbol_KeepSize         <> "" then symbol.KeepSize           = symbol_KeepSize        
						end if 'diagram.findsymbol(table) is nothing then
					end if  'table_index <> -1 then
				end if   'symbol_objecttype = "TableSymbol") 
				
				if symbol_objecttype = "ViewSymbol" then
					view_index = get_view_index(excel_get_cell_value(excel_row_nr, 3))
					if view_index <> -1 then
						set view = activemodel.views.item(view_index)
'output view.code
						if diagram.findsymbol(view) is nothing then
							output excel_row_nr & "> "& diagram.code &  " add view symbol for  " &  excel_get_cell_value(excel_row_nr, 3)
							set symbol = diagram.attachobject(view)
							if symbol_rect_top         <> "" then symbol.rect.top           = symbol_rect_top        
							if symbol_rect_bottom      <> "" then symbol.rect.bottom        = symbol_rect_bottom     
							if symbol_rect_left        <> "" then symbol.rect.left          = symbol_rect_left       
							if symbol_rect_right       <> "" then symbol.rect.right         = symbol_rect_right      
							if symbol_linecolor        <> "" then symbol.linecolor          = symbol_linecolor       
							if symbol_fillcolor        <> "" then symbol.fillcolor          = symbol_fillcolor       
							if symbol_gradientfillmode <> "" then symbol.gradientfillmode   = symbol_gradientfillmode
							if symbol_AutoAdjustToText <> "" then symbol.AutoAdjustToText   = symbol_AutoAdjustToText                                     
							if symbol_KeepAspect       <> "" then symbol.KeepAspect         = symbol_KeepAspect                                           
							if symbol_KeepCenter       <> "" then symbol.KeepCenter         = symbol_KeepCenter                                           
							if symbol_KeepSize         <> "" then symbol.KeepSize           = symbol_KeepSize                                             
						end if 'diagram.findsymbol(table) is nothing then
					end if  'table_index <> -1 then
				end if   'symbol_objecttype = "TableSymbol") then   
				
				if symbol_objecttype = "ReferenceSymbol" then
					
					reference_index = get_reference_index(excel_get_cell_value(excel_row_nr, 3))
					if reference_index <> -1 then
						set reference = activemodel.references.item(reference_index)
						if diagram.findsymbol(reference) is nothing then
							output excel_row_nr & "> "& diagram.code &  " add reference symbol for  " &  excel_get_cell_value(excel_row_nr, 3)
							set symbol = diagram.attachlinkobject(reference)
							if excel_get_cell_value(excel_row_nr, 5) <> "" then symbol.rect.top           = excel_get_cell_value(excel_row_nr, 5)
							if excel_get_cell_value(excel_row_nr, 6) <> "" then symbol.rect.bottom        = excel_get_cell_value(excel_row_nr, 6)
'if excel_get_cell_value(excel_row_nr, 7) <> "" then symbol.rect.left          = excel_get_cell_value(excel_row_nr, 7)
'if excel_get_cell_value(excel_row_nr, 8) <> "" then symbol.rect.right         = excel_get_cell_value(excel_row_nr, 8)
							if excel_get_cell_value(excel_row_nr, 9) <> "" then symbol.linecolor          = excel_get_cell_value(excel_row_nr, 9)
							
' if excel_get_cell_value(excel_row_nr, 9) <> "" then symbol.linecolor          =  12648384  ' excel_get_cell_value(excel_row_nr, 9)
							
							end  if 'if diagram.findsymbol(reference) is nothing then                     
						end if     ' reference_index <> -1 then
					end if  'symbol_objecttype = "ReferenceSymbol") then
					
					
					if symbol_objecttype = "ViewReferenceSymbol" then
						
						viewreference_index = get_viewreference_index(excel_get_cell_value(excel_row_nr, 3))
						
						output  "hello "&excel_get_cell_value(excel_row_nr, 3) &" index= " & viewreference_index
						
						if viewreference_index <> -1 then
							set viewreference = activemodel.viewreferences.item(viewreference_index)
							
							if diagram.findsymbol(viewreference) is nothing then
								output excel_row_nr & "> "& diagram.code &  " add viewreference symbol for  " &  excel_get_cell_value(excel_row_nr, 3)
								set symbol = diagram.attachlinkobject(viewreference)
								if excel_get_cell_value(excel_row_nr, 5) <> "" then symbol.rect.top           = excel_get_cell_value(excel_row_nr, 5)
								if excel_get_cell_value(excel_row_nr, 6) <> "" then symbol.rect.bottom        = excel_get_cell_value(excel_row_nr, 6)
								if excel_get_cell_value(excel_row_nr, 9) <> "" then symbol.linecolor          = excel_get_cell_value(excel_row_nr, 9)
								end  if 'if diagram.findsymbol(reference) is nothing then                     
							end if     ' reference_index <> -1 then
							
							
						end if  'symbol_objecttype = "ReferenceSymbol") then
						
						
						
					end if '(not curr_line_empty) then
					excel_row_nr = excel_row_nr + 1
					prev_line_empty = curr_line_empty
					curr_line_empty = (excel_get_cell_value(excel_row_nr, 1) = "")
					
				wend  'while not (prev_line_empty and curr_line_empty)
				
			end if  'par_symbols 
			
'----------------------------------------------------------------
' setters for physical model only
			
			
			PK_teller = 0  
			For Each table In activemodel.tables
				If Not table.isshortcut Then
					
					For Each key In table.keys
						If key.primary  = true Then
							
'pktabnr  = get_table_index(table.code)
							
							if pk_teller < 10 then pk_teller_text = "PK00"&pk_teller
							if (pk_teller > 10 )and ( pk_teller  < 100 ) then pk_teller_text = "PK0"&pk_teller
							if (pk_teller > 100 )and ( pk_teller  < 1000 ) then pk_teller_text = "PK"&pk_teller
							
							new_key_name        = pk_teller_text&"_"& table.code
							new_key_code        = pk_teller_text&"_"& table.code
							new_constraint_name = pk_teller_text
'output "--------------------" & table.code
'output key.name & " - " & new_key_name
'output key.code & " - " & new_key_code
'output key.constraintname & " - " & new_constraint_name
							key.name = new_key_name
							key.code = new_key_code
							key.constraintname = new_constraint_name
							PK_teller = PK_teller + 1
							
						End If
					Next
				End If
			Next 'For Each table In activemodel.tables
			
			
			AK_teller = 0            
			For Each table In activemodel.tables
				If Not table.isshortcut Then
					For Each key In table.keys
						If  key.primary = false  Then
							
' bktabnr  = get_table_index(table.code)
							
							if ak_teller < 10 then ak_teller_text = "AK00"&AK_teller
							if (ak_teller > 10 )and ( ak_teller  < 100 ) then ak_teller_text = "AK0"&AK_teller
							if (ak_teller > 100 )and ( ak_teller  < 1000 ) then ak_teller_text = "AK"&AK_teller
							
							new_key_name = ak_teller_text & table.code
							new_key_code = ak_teller_text& table.code            
							new_constraint_name = ak_teller_text
							
' output "--------------------" & table.code
' output key.name & " - " & new_key_name
' output key.code & " - " & new_key_code
' output key.constraintname & " - " & new_constraint_name
							
							key.name = new_key_name
							key.code = new_key_code
							key.constraintname = new_constraint_name
							
							AK_teller = AK_teller + 1
							
						End If
					Next 'For Each key In table.keys
				End If
			Next  'For Each table In activemodel.tables    
			
			
			FK_teller = 0
			
			For Each reference In activemodel.references
				If Not reference.isshortcut Then
					
'childtabnr  = get_table_index(reference.childtable.code )
'parenttabnr = get_table_index(reference.parenttable.code) 
					
					if fk_teller < 10 then fk_teller_text = "FK00"&FK_teller
					if (fk_teller > 10 )and ( fk_teller  < 100 ) then fk_teller_text = "FK0"&FK_teller
					if (fk_teller > 100 )and ( fk_teller  < 1000 ) then fk_teller_text = "FK"&FK_teller
					
					
					new_key_name =fk_teller_text & "_" & reference.childtable.name & "_" & reference.ParentTable.name
					new_key_code =fk_teller_text & "_" & reference.childtable.code & "_" & reference.ParentTable.code
					new_constraint_name = fk_teller_text
					
' output "--------------------" & reference.code
' output reference.code & " - " & new_key_code
' output reference.name & " - " & new_key_name
' output reference.foreignkeyconstraintname & " - " & new_constraint_name
					reference.code = new_key_code
					reference.name = new_key_name
					reference.foreignkeyconstraintname = new_constraint_name
					
					FK_teller = FK_teller + 1
					
				End If
			Next
'----------------------------------------------------------------   
			
'******************** DEZE aan het einde gezet ivm regelmatig de poppen aan het dansen.
			
			if par_vwref_joins then 
				
				set dummy = excel_select_worksheet(18)
				
				excel_row_nr = 3
				prev_line_empty = false
				curr_line_empty = (excel_get_cell_value(excel_row_nr, 1) = "")
				
				while not (prev_line_empty and curr_line_empty)
					if (not curr_line_empty) then
						
'getter 
						vwref_name                             = excel_get_cell_value(excel_row_nr,1 ) '   reference.name                1 
						vwref_childview_name                   = excel_get_cell_value(excel_row_nr,2 ) '   reference.childtable.name     2 
						vwrefjoin_childviewcolumn_name         = excel_get_cell_value(excel_row_nr,3 ) '   join.childtablecolumn.name    3 
						vwref_parentview_name                  = excel_get_cell_value(excel_row_nr,4 ) '   reference.parenttable.name    4 
						vwrefjoin_parentviewcolumn_name        = excel_get_cell_value(excel_row_nr,5 ) '   join.parenttablecolumn.name   5 
						vwref_code                             = excel_get_cell_value(excel_row_nr,6 ) '   reference.code                6 
						vwref_childview_code                   = excel_get_cell_value(excel_row_nr,7 ) '   reference.childtable.code     7 
						vwrefjoin_childviewcolumn_code         = excel_get_cell_value(excel_row_nr,8 ) '   join.childtablecolumn.code    8 
						vwref_parentview_code                  = excel_get_cell_value(excel_row_nr,9 ) '   reference.parenttable.code    9 
						vwrefjoin_parentviewcolumn_code        = excel_get_cell_value(excel_row_nr,10) '   join.parenttablecolumn.code   10
						
						output excel_row_nr &"> add vwrefjoin " & vwref_childview_name  & "." & vwrefjoin_childviewcolumn_code  & "->" & vwref_parentview_name & "." &  vwrefjoin_parentviewcolumn_code   
						
						for each vwref in activemodel.viewreferences   
							if not vwref.isshortcut then
								
								if vwref.code = vwref_code    then 
									output vwref.code 
									for each parentcol in vwref.tableview1.Columns
										
										output vwrefjoin_parentviewcolumn_code
										
										if  parentcol.code  =  vwrefjoin_parentviewcolumn_code then
											exit for
										end if
									next 'parcol
									
									for each childcol in vwref.tableview2.Columns
										if  childcol.code  =  vwrefjoin_childviewcolumn_code then
											exit for
										end if
									next 'childcol
									
									
'setter
									set newjoin   = vwref.joins.CreateNew()  
									with newjoin
										.column1     =  parentcol   
										.column2      =  childcol
									end with  
								end if  '  reference.code = reference_cod
							end if   'not reference.isshortcut then
						next 'reference   
						
					end if  '(not curr_line_empty) then
					excel_row_nr = excel_row_nr + 1
					prev_line_empty = curr_line_empty
					curr_line_empty = (excel_get_cell_value(excel_row_nr, 1) = "")      
					
				wend 'while not (prev_line_empty and curr_line_empty)
				
				
			end if  'par_vwref_joins then
			
			
'----------------------------------------------------------------       
			
'finally Check check double check
			entities_teller        = 0
			attributes_teller      = 0
			relations_teller       = 0
			joins_teller           = 0
			diagram_teller         = 0
			symbol_teller          = 0 
			view_teller            = 0
			
			for each table in activemodel.tables
				if not table.isshortcut then
					entities_teller = entities_teller + 1   
					
					for each column in table.columns
						attributes_teller = attributes_teller + 1   
					next
				end if
			next
			output  "aantal entities   = "& entities_teller & " verwacht =  " & verwacht_aantal_entities        
			output  "aantal attributes = "& attributes_teller   & " verwacht =  " & verwacht_aantal_attributes    
			
			for each reference in activemodel.references   
				if not reference.isshortcut then
					
					relations_teller = relations_teller  + 1
					for each refjoin in reference.joins      
						joins_teller = joins_teller + 1           
					next ' refjoin                
				end if   'not reference.isshortcut then
			next 'reference  
			
			output  "aantal relations = "& relations_teller & " verwacht =  " & verwacht_aantal_relations        
			output  "aantal joins = "& joins_teller    & " verwacht =  " & verwacht_aantal_joins     
			
			
			for each diagram in activemodel.physicaldiagrams
				diagram_teller = diagram_teller + 1
				
				for each s1 in diagram.symbols
					symbol_teller = symbol_teller  + 1
					
					if s1.objecttype = "TableSymbol"  then
' format font 
						dim font_list          
						font_list = "STRN 0 "&lettertype&",N" 
						font_list = font_list + vbCrLF + "DISPNAME 0 "&lettertype&",N" 
						font_list = font_list + vbCrLF + "OWNRDISPNAME 0 "&lettertype&",N" 
						font_list = font_list + vbCrLF + "Columns 0 "&lettertype&",N" 
						font_list = font_list + vbCrLF + "TablePkColumns " & CStr(RGB(255,0,0)) &" "&lettertype&",U"   ' rood
						font_list = font_list + vbCrLF + "TableFkColumns " & CStr(RGB(0,0,255)) &" "&lettertype&",N"   ' blauw
						font_list = font_list + vbCrLF + "Keys 0 "&lettertype&",N"
						font_list = font_list + vbCrLF + "Indexes 0 "&lettertype&",N" 
						font_list = font_list + vbCrLF + "Triggers 0 "&lettertype&",N" 
						font_list = font_list + vbCrLF + "LABL 0 "&lettertype&",N" 
						s1.fontlist = font_list  'change font list
					end if 's1.objecttype = "Table"  
					
					IF s1.objecttype = "ReferenceSymbol"  THEN   
							font_list = "SOURCE 0 "&lettertype&",N" 
							font_list = font_list + vbCrLF + "CENTER 0 "&lettertype&",N" 
						font_list = font_list + vbCrLF + "DESTINATION 0 "&lettertype&",N" 
						s1.fontlist = font_list
							END IF  ' s1.objecttype = "ReferenceSymbol"
					
					
					
					
				next 's1 in diagram.symbols
			next 'diagram in activemodel.physicaldiagrams  
			
'	output  "aantal diagrams = "& diagram_teller & " verwacht =  " & verwacht_aantal_diagrams        
'	output  "aantal symbols  = "& symbol_teller  & " verwacht =  " & verwacht_aantal_symbols   
			
			for each view in activemodel.views
				if not view.isshortcut then
					view_teller = view_teller + 1   
				end if
			next
'output  "aantal views   = "& view_teller & " verwacht =  " & verwacht_aantal_views        
			
'******************** 
			
'>>> close the excel file
			
			Dim dg
			Set dg = activemodel.PhysicalDiagrams.Item(0)
			If dg.code = "tt" then
				dg.delete()
			end if
			
			
'  set dummy = excel_autosave_file
			set dummy = excel_close_file()   
			
'Manually kill Excel process with..  Windows taskmanager  end-process.  
'(In case you don't reach this point. (twents: als de poppen aan het dansen zijn)
			
		end sub  ' the end of main hope the run was okay. 
		
		
'klaar met processing
		Output "hope the run was ok! "  
		Output
		output "VB Script started on   " & vbstartdate & " at " & vbstarttime   
'  msgbox "klaar met processing. hope the run was okay :"
		
'******************** 
'# Structured programming:
'# Entire program logic modularized in functions.
'******************** 
'------------------------------------------------------------------------------
'#Powerdesigner is te beschouwen als een [ObjectOriented XML database] 
'#De OO getters setters staan hieronder
'------------------------------------------------------------------------------
		
'********** excel functions **********
' cc.wust@belastingdienst.nl
'>>> functions that are called by other functions
'excel rows and columns are numbered 1, 2, 3, ...
'>>> definitions
		dim excel_obj
		excel_colorindex_nocolor = xlColorIndexNone
		excel_colorindex_black = 1
		excel_colorindex_white = 2
		excel_colorindex_red = 3
		excel_colorindex_green = 4
		excel_colorindex_blue= 5
		excel_colorindex_yellow= 6
		excel_colorindex_magenta = 7
		excel_colorindex_cyan= 8
'>>> functions that are called by other functions
		function excel_get_row(excel_row_nr)
			excel_get_row = cstr(excel_row_nr)
		end function
		function excel_get_rows(excel_row_nr1, excel_row_nr2)
			excel_get_rows = excel_get_row(excel_row_nr1) & ":" & excel_get_row(excel_row_nr2)
		end function
		function excel_get_column(excel_column_nr)
			letter1 = (excel_column_nr - 1) \ 26
			letter2 = ((excel_column_nr - 1) mod 26) + 1
			if letter1 = 0 then
				excel_get_column = chr(64 + letter2)
			else
				excel_get_column = chr(64 + letter1) & chr(64 + letter2)
			end if
		end function
		function excel_get_columns(excel_column_nr1, excel_column_nr2)
			excel_get_columns = excel_get_column(excel_column_nr1) & ":" & excel_get_column(excel_column_nr2)
		end function
		function excel_get_cell(excel_row_nr, excel_column_nr)
			excel_get_cell = excel_get_column(excel_column_nr) & excel_get_row(excel_row_nr)
		end function
		function excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)
			excel_get_cells = excel_get_column(excel_column_nr1) & excel_get_row(excel_row_nr1) & ":" & excel_get_column(excel_column_nr2) & excel_get_row(excel_row_nr2)
		end function
'>>> functions to set or read the value of a cell
		function excel_set_cell_value(excel_row_nr, excel_column_nr, value)
			excel_obj.range(excel_get_cell(excel_row_nr, excel_column_nr)).value = value
			set excel_set_cell_value = nothing
		end function
		function excel_get_cell_value(excel_row_nr, excel_column_nr)
			excel_get_cell_value = excel_obj.range(excel_get_cell(excel_row_nr, excel_column_nr)).value
		end function
'>>> i/o functions
		function excel_new_file(visible, nr_sheets)
			set excel_obj = createobject("excel.application")
			excel_obj.visible = visible
			excel_obj.sheetsinnewworkbook = nr_sheets
			excel_obj.workbooks.add
			set excel_new_file = nothing
		end function
		function excel_load_file(visible, filename)
			set excel_obj = createobject("excel.application")
			excel_obj.visible = visible
			excel_obj.workbooks.open(filename)
			excel_obj.worksheets(1).select
			set excel_load_file = nothing
		end function
		
		function excel_save_file()
			excel_obj.activeworkbook.save
			set excel_save_file = nothing
		end function
		
		function excel_autosave_file()
			DisplayAlerts = False
			excel_obj.activeworkbook.save
			displayAlerts = True 
			set excel_autosave_file = nothing
			
		end function
		
		function excel_save_as_file(filename)
			excel_obj.activeworkbook.saveas(filename)
			set excel_save_as_file = nothing
		end function
		
		function excel_close_file()
			
			excel_obj.activeworkbook.Saved = true
			excel_obj.activeworkbook.close
			
			set excel_close_file = nothing
		end function
'>>> worksheet functions
		function excel_select_worksheet(worksheet_nr)
			excel_obj.worksheets(worksheet_nr).select
			set excel_select_worksheet = nothing
		end function
		function excel_set_worksheet_name(worksheet_nr, worksheet_name)
			excel_obj.worksheets(worksheet_nr).name = worksheet_name
			set excel_set_worksheet_name = nothing
		end function
		function excel_get_worksheet_name(worksheet_nr)
			excel_get_worksheet_name = excel_obj.worksheets(worksheet_nr).name
		end function
'>>> layout functions
		function excel_set_cursor(excel_row_nr, excel_column_nr)
			excel_obj.range(excel_get_cell(excel_row_nr, excel_column_nr)).select
			set excel_set_cursor = nothing
		end function
		function excel_merge_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)
			excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).select
			excel_obj.selection.merge
			set excel_merge_cells = nothing
		end function
		function excel_set_zoom(zoom_percentage)
			excel_obj.activewindow.zoom = zoom_percentage
			set excel_set_zoom = nothing
		end function
		function excel_autofit_columns(excel_column_nr1, excel_column_nr2)
			excel_obj.columns(excel_get_columns(excel_column_nr1, excel_column_nr2)).autofit
			set excel_autofit_columns = nothing
		end function
		function excel_freeze_row(excel_row_nr)
			excel_obj.rows(excel_get_rows(excel_row_nr, excel_row_nr)).select
			excel_obj.activewindow.freezepanes = true
			set excel_freeze_row = nothing
		end function
		function excel_freeze_column(excel_column_nr)
			excel_obj.columns(excel_get_columns(excel_column_nr, excel_column_nr)).select
			excel_obj.activewindow.freezepanes = true
			set excel_freeze_column = nothing
		end function
		function excel_freeze_cell(excel_row_nr, excel_column_nr)
			excel_obj.range(excel_get_cell(excel_row_nr, excel_column_nr)).select
			excel_obj.activewindow.freezepanes = true
			set excel_freeze_cell = nothing
		end function
		function excel_set_cells_background_color(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2, excel_colorindex)
			excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).interior.colorindex = excel_colorindex
			set excel_set_cells_background_color = nothing
		end function
		function excel_set_cells_border_color(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2, excel_colorindex)
			excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).select
			excel_obj.selection.borders.linestyle = xlContinuous
			excel_obj.selection.borders.colorindex = excel_colorindex
			set excel_set_cells_border_color = nothing
		end function
		function excel_set_cells_font_color(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2, excel_colorindex)
			excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).font.colorindex = excel_colorindex
			set excel_set_cells_font_color = nothing
		end function
		function excel_set_cells_bold(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2, activate)
			excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).font.bold = activate
			set excel_set_cells_bold = nothing
		end function
		function excel_set_cells_italic(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2, activate)
			excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).font.italic = activate
			set excel_set_cells_italic = nothing
		end function
		function excel_set_cells_underline(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2, activate)
			excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).font.underline = activate
			set excel_set_cells_underline = nothing
		end function
		
		
'=====================================
		function excel_unhide_worksheets()
			
			excel_obj.worksheets(1).visible = true  'titel en versiebeheer
			excel_obj.worksheets(2).visible = true  'entities
			excel_obj.worksheets(3).visible = true  'attributes
			excel_obj.worksheets(4).visible = true  'relations
			excel_obj.worksheets(5).visible = true  'joins
			excel_obj.worksheets(6).visible = true  'views
			excel_obj.worksheets(7).visible = true  'view attributes  
			excel_obj.worksheets(8).visible = true  'diagrams
			excel_obj.worksheets(9).visible = true  'symbols
			
			excel_obj.worksheets(10).visible = true ' HUB controle telling
			excel_obj.worksheets(11).visible = true ' LNK controle telling
			excel_obj.worksheets(12).visible = true ' SAT ... eventhook
			excel_obj.worksheets(13).visible = true ' REF controle telling
			excel_obj.worksheets(14).visible = true ' RFH controle telling
			excel_obj.worksheets(15).visible = true'
			excel_obj.worksheets(16).visible = true '
			excel_obj.worksheets(17).visible = true 'View references
			excel_obj.worksheets(18).visible = true 'View reference Joins
			
			set excel_unhide_worksheets = nothing 
			
		end function 'excel_hide_worksheets  
		
		
		
		function excel_unhide_columns()
			
'------------------------entities
			excel_obj.worksheets(2).columns(1 ).hidden=false     ' table_annotation                 table.annotation                  
			excel_obj.worksheets(2).columns(2 ).hidden=false     ' table_annotation                 table.annotation                   
			excel_obj.worksheets(2).columns(3 ).hidden=false     ' table_annotation                 table.annotation                   
			excel_obj.worksheets(2).columns(4 ).hidden=false     ' table_beginscript                table.beginscript                  
			excel_obj.worksheets(2).columns(5 ).hidden=false     ' table_name                       table.name                         
			excel_obj.worksheets(2).columns(6 ).hidden=false     ' table_CheckConstraintName        table.CheckConstraintName          
			excel_obj.worksheets(2).columns(7 ).hidden=false     ' table_description                table.description                  
			excel_obj.worksheets(2).columns(8 ).hidden=false    ' table_endscript                  table.endscript                    
			excel_obj.worksheets(2).columns(9 ).hidden=false     ' table_code                       table.code                         
			excel_obj.worksheets(2).columns(10).hidden=false     ' table_stereotype                 table.stereotype                   
			excel_obj.worksheets(2).columns(11).hidden=false     ' table_type                       table.type                         
			excel_obj.worksheets(2).columns(12).hidden=false     ' table_comment                    table.comment                      
'------------------------attributes
			excel_obj.worksheets(3).columns(1 ).hidden=false     ' column_annotation                column.annotation                  
			excel_obj.worksheets(3).columns(2 ).hidden=false     ' column_annotation                column.annotation                  
			excel_obj.worksheets(3).columns(3 ).hidden=false     ' column_annotation                column.annotation                  
			excel_obj.worksheets(3).columns(4 ).hidden=false     ' table_name                       table.name                         
			excel_obj.worksheets(3).columns(5 ).hidden=false     ' column_name                      column.name                        
			excel_obj.worksheets(3).columns(6 ).hidden=false     ' column_format                    column.format                      
			excel_obj.worksheets(3).columns(7 ).hidden=false     ' column_physicaloptions           column.physicaloptions             
			excel_obj.worksheets(3).columns(8 ).hidden=false     ' column_lowvalue                  column.lowvalue                    
			excel_obj.worksheets(3).columns(9 ).hidden=false     ' column_highvalue                 column.highvalue                   
			excel_obj.worksheets(3).columns(10).hidden=false     ' column_unit                      column.unit                        
			excel_obj.worksheets(3).columns(11).hidden=false     ' column_primary                   column.primary                     
			excel_obj.worksheets(3).columns(12).hidden=false     ' column_mandatory                 column.mandatory                   
			excel_obj.worksheets(3).columns(13).hidden=false     ' column_nospace                   column.nospace                     
			excel_obj.worksheets(3).columns(14).hidden=false     ' column_description               column.description                 
			excel_obj.worksheets(3).columns(15).hidden=false     ' table_endscript                  table.endscript                    
			excel_obj.worksheets(3).columns(16).hidden=false     ' table_code                       table.code                         
			excel_obj.worksheets(3).columns(17).hidden=false     ' column_code                      column.code                        
			excel_obj.worksheets(3).columns(18).hidden=false     ' column_datatype                  column.datatype                    
			excel_obj.worksheets(3).columns(19).hidden=false     ' column_stereotype                column.stereotype                  
			excel_obj.worksheets(3).columns(20).hidden=false     ' column_ComputedExpression        column.ComputedExpression          
			excel_obj.worksheets(3).columns(21).hidden=false     ' column_comment                   column.comment                     
'------------------------relations
			excel_obj.worksheets(4).columns(1 ).hidden=false     ' reference_annotation             reference.annotation               
			excel_obj.worksheets(4).columns(2 ).hidden=false     ' reference_annotation             reference.annotation               
			excel_obj.worksheets(4).columns(3 ).hidden=false     ' reference_annotation             reference.annotation               
			excel_obj.worksheets(4).columns(4 ).hidden=false     ' reference_name                   reference.name                     
			excel_obj.worksheets(4).columns(5 ).hidden=false     ' reference_childtable_name        reference.childtable.name          
			excel_obj.worksheets(4).columns(6 ).hidden=false     ' reference_parenttable_name       reference.parenttable.name         
			excel_obj.worksheets(4).columns(7 ).hidden=false     ' reference_cardinality            reference.cardinality              
			excel_obj.worksheets(4).columns(8 ).hidden=false     ' reference_DeleteConstraint       reference.DeleteConstraint         
			excel_obj.worksheets(4).columns(9 ).hidden=false     ' reference_UpdateConstraint       reference.UpdateConstraint         
			excel_obj.worksheets(4).columns(10).hidden=false     ' reference_ChangeParentAllowed    reference.ChangeParentAllowed      
			excel_obj.worksheets(4).columns(11).hidden=false     ' reference_childrole              reference.childrole                
			excel_obj.worksheets(4).columns(12).hidden=false     ' reference_parentrole             reference.parentrole               
			excel_obj.worksheets(4).columns(13).hidden=false     ' reference_comment                reference.comment                  
			excel_obj.worksheets(4).columns(14).hidden=false     ' reference_code                   reference.code                     
			excel_obj.worksheets(4).columns(15).hidden=false     ' reference_childtable_code        reference.childtable.code          
			excel_obj.worksheets(4).columns(16).hidden=false     ' reference_parenttable_code       reference.parenttable.code         
			excel_obj.worksheets(4).columns(17).hidden=false     ' reference_stereotype             reference.stereotype               
			excel_obj.worksheets(4).columns(18).hidden=false     ' reference_ImplementationType     reference.ImplementationType       
			excel_obj.worksheets(4).columns(19).hidden=false     ' reference_description            reference.description              
			excel_obj.worksheets(4).columns(20).hidden=false     ' reference_JoinExpression         reference.JoinExpression           
			excel_obj.worksheets(4).columns(21).hidden=false     ' reference_ForeignKeyColumnList   reference.ForeignKeyColumnList     
			excel_obj.worksheets(4).columns(22).hidden=false     ' reference_ParentKeyColumnList    reference.ParentKeyColumnList      
			excel_obj.worksheets(4).columns(23).hidden=false     ' reference_foreignkeyconstraintnamreference.foreignkeyconstraintnamee
'------------------------joins
			excel_obj.worksheets(5).columns(1 ).hidden=false     ' reference_name                   reference.name                     
			excel_obj.worksheets(5).columns(2 ).hidden=false     ' reference_childtable_name        reference.childtable.name          
			excel_obj.worksheets(5).columns(3 ).hidden=false     ' join_childtablecolumn_name       join.childtablecolumn.name         
			excel_obj.worksheets(5).columns(4 ).hidden=false     ' reference_parenttable_name       reference.parenttable.name         
			excel_obj.worksheets(5).columns(5 ).hidden=false     ' join_parenttablecolumn_name      join.parenttablecolumn.name        
			excel_obj.worksheets(5).columns(6 ).hidden=false     ' reference_code                   reference.code                     
			excel_obj.worksheets(5).columns(7 ).hidden=false     ' reference_childtable_code        reference.childtable.code          
			excel_obj.worksheets(5).columns(8 ).hidden=false     ' join_childtablecolumn_code       join.childtablecolumn.code         
			excel_obj.worksheets(5).columns(9 ).hidden=false     ' reference_parenttable_code       reference.parenttable.code         
			excel_obj.worksheets(5).columns(10).hidden=false     ' join_parenttablecolumn_code      join.parenttablecolumn.code        
'------------------------views                          
			excel_obj.worksheets(6).columns(1 ).hidden=false     ' view_annotation                  view.annotation                    
			excel_obj.worksheets(6).columns(2 ).hidden=false     ' view_annotation                  view.annotation                    
			excel_obj.worksheets(6).columns(3 ).hidden=false     ' view_annotation                  view.annotation                    
			excel_obj.worksheets(6).columns(4 ).hidden=false     ' view_code                        view.code                          
			excel_obj.worksheets(6).columns(5 ).hidden=false     ' view_name                        view.name                          
			excel_obj.worksheets(6).columns(6 ).hidden=false     ' view_stereotype                  view.stereotype                    
			excel_obj.worksheets(6).columns(7 ).hidden=false     ' view_comment                     view.comment                       
			excel_obj.worksheets(6).columns(8 ).hidden=false     ' view_type                        view.type                          
			excel_obj.worksheets(6).columns(9 ).hidden=false     ' view_description                 view.description                   
			excel_obj.worksheets(6).columns(10).hidden=false     ' view_sqlquery                    view.sqlquery                      
'------------------------viewscolumns                       
			excel_obj.worksheets(7).columns(1 ).hidden=false     ' column_annotation                column.annotation                  
			excel_obj.worksheets(7).columns(2 ).hidden=false     ' column_annotation                column.annotation                  
			excel_obj.worksheets(7).columns(3 ).hidden=false     ' column_annotation                column.annotation                  
			excel_obj.worksheets(7).columns(4 ).hidden=false     ' view_code                        view.code                          
			excel_obj.worksheets(7).columns(5 ).hidden=false     ' column_code                      column.code                        
			excel_obj.worksheets(7).columns(6 ).hidden=false     ' column_name                      column.name                        
			excel_obj.worksheets(7).columns(7 ).hidden=false     ' column_format                    column.format                      
			excel_obj.worksheets(7).columns(8 ).hidden=false     ' column_unit                      column.unit                        
			excel_obj.worksheets(7).columns(9 ).hidden=false     ' column_lowvalue                  column.lowvalue                    
			excel_obj.worksheets(7).columns(10).hidden=false     ' column_highvalue                 column.highvalue                   
			excel_obj.worksheets(7).columns(11).hidden=false     ' column_mandatory                 column.mandatory                   
			excel_obj.worksheets(7).columns(12).hidden=false     ' column_stereotype                column.stereotype                  
			excel_obj.worksheets(7).columns(13).hidden=false     ' column_comment                   column.comment                     
			excel_obj.worksheets(7).columns(14).hidden=false     ' column_description               column.description                 
'------------------------diagrams
			excel_obj.worksheets(8).columns(1 ).hidden=false     ' diagram_code                     diagram.code                       
			excel_obj.worksheets(8).columns(2 ).hidden=false     ' diagram_name                     diagram.name                       
			excel_obj.worksheets(8).columns(3 ).hidden=false     ' diagram_pageformat               diagram.pageformat                 
			excel_obj.worksheets(8).columns(4 ).hidden=false     ' diagram_pageorientation          diagram.pageorientation            
'------------------------symbols
			excel_obj.worksheets(9).columns(1 ).hidden=false     ' diagram_code                     diagram.code                       
			excel_obj.worksheets(9).columns(2 ).hidden=false     ' symbol_objecttype                symbol.objecttype                  
			excel_obj.worksheets(9).columns(3 ).hidden=false     ' symbol_code                      symbol.code                        
			excel_obj.worksheets(9).columns(4 ).hidden=false     ' symbol_name                      symbol.name                        
			excel_obj.worksheets(9).columns(5 ).hidden=false     ' symbol_rect_top                  symbol.rect.top                    
			excel_obj.worksheets(9).columns(6 ).hidden=false     ' symbol_rect_bottom               symbol.rect.bottom                 
			excel_obj.worksheets(9).columns(7 ).hidden=false     ' symbol_rect_left                 symbol.rect.left                   
			excel_obj.worksheets(9).columns(8 ).hidden=false     ' symbol_rect_right                symbol.rect.right                  
			excel_obj.worksheets(9).columns(9 ).hidden=false     ' symbol_linecolor                 symbol.linecolor                   
			excel_obj.worksheets(9).columns(10).hidden=false     ' symbol_fillcolor                 symbol.fillcolor                   
			excel_obj.worksheets(9).columns(11).hidden=false     ' symbol_gradientfillmode          symbol.gradientfillmode            
			excel_obj.worksheets(9).columns(12).hidden=false     ' symbol_AutoAdjustToText          symbol.AutoAdjustToText            
			excel_obj.worksheets(9).columns(13).hidden=false     ' symbol_KeepAspect                symbol.KeepAspect                  
			excel_obj.worksheets(9).columns(14).hidden=false     ' symbol_KeepCenter                symbol.KeepCenter                  
			excel_obj.worksheets(9).columns(15).hidden=false     ' symbol_KeepSize                  symbol.KeepSize                    
			
			
			set excel_unhide_columns = nothing 
		end function 'excel_unhide_columns
		
		
		
		
		
'------------------------------------------------------------------------------
'********** powerdesigner functions **********
'------------------------------------------------------------------------------
'>>> index functions
		function get_table_index(table_code)
			i = 0
			nr_tables = activemodel.tables.count
			for i = 0 to nr_tables - 1
				set table = activemodel.tables.item(i)
				if ((table.isshortcut = false) and (table.code = table_code)) then
					exit for
				end if
			next
			if i = nr_tables then i = -1
			get_table_index = i
		end function
		function get_view_index(view_code)
			i = 0
			nr_views = activemodel.views.count
			for i = 0 to nr_views - 1
				set view = activemodel.views.item(i)
				if ((view.isshortcut = false) and (view.code = view_code)) then
					exit for
				end if
			next
			if i = nr_views then i = -1
			get_view_index = i
		end function
		
		function get_column_index(table_index, column_code)
			i = 0
			set table = activemodel.tables.item(table_index)
			nr_columns = table.columns.count
			for i = 0 to nr_columns - 1
				if table.columns.item(i).code = column_code then
					exit for
				end if
			next
			if i = nr_columns then i = -1
			get_column_index = i
		end function
		
		function get_reference_index(reference_code)
			i = 0
			nr_references = activemodel.references.count
			for i = 0 to nr_references - 1
				set reference = activemodel.references.item(i)
				if ((reference.isshortcut = false) and (reference.code = reference_code)) then
					exit for
				end if
			next
			if i = nr_references then i = -1
			get_reference_index = i
		end function
		
		
		function get_Viewreference_index(viewreference_code)
			i = 0
			nr_viewreferences = activemodel.viewreferences.count
			for i = 0 to nr_viewreferences - 1
				set viewreference = activemodel.viewreferences.item(i)
				if ((viewreference.isshortcut = false) and (viewreference.code = viewreference_code)) then
					exit for
				end if
			next
			if i = nr_viewreferences then i = -1
			get_viewreference_index = i
		end function
		
		
		
		function get_join_index(reference_index, parentcolumn_code)
			i = 0
			set reference = activemodel.references.item(reference_index)
			nr_joins = reference.joins.count
			for i = 0 to nr_joins - 1
				if reference.joins.item(i).parenttablecolumn.code = parentcolumn_code then
					exit for
				end if
			next
			if i = nr_joins then i = -1
			get_join_index = i
		end function
		
		function get_diagram_index(diagram_code)
			i = 0
			nr_diagrams = activemodel.physicaldiagrams.count
			for i = 0 to nr_diagrams - 1
				if ucase(activemodel.physicaldiagrams.item(i).code) = ucase(diagram_code) then
					exit for
				end if
			next
			if i = nr_diagrams then i = -1
			get_diagram_index = i
		end function
		
		function  excel_set_autofilter(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)
			excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).AutoFilter
			set excel_set_autofilter = nothing
		end function
		
		
'Voeg een kolom toe aan de "BK" alternate (business) key van zijn tabel 
		private sub set_businesskey(pCol)
			
			dim keyBK
			set keyBK=Nothing
			for each key in pCol.Table.Keys
				if key.code="BK" then set keyBK=key
			next
			if keyBK is nothing then
				set keyBK=pCol.Table.Keys.CreateNew
				keyBK.code="BK"
				keyBK.name="BK"
			end if
			
			pCol.keys.add keyBK
		end sub
		
' END OF FUNCTIONS
'------------------------------------------------------------------------------
' # VB kent (helaas) geen libraries dus voeg ze hieronder maar toe
'------------------------------------------------------------------------------
		
'------------------------------------------------------------------------------
' Powerdesigner is te beschouwen als een ObjectOriented XML database. 
' Via getters-setters kunnen data geimporteerd en worden geexporteerd naar excel als ware het een .DDL
' Daarmee zijn we in staat om het pwdes (PSM=platformspecifieke) datamodel steeds 
' uit te wisselen met PIM=platform onafhankelijke modelmet voldoende 'vieuwpoints' 
' zodat het als koppelvlakbeschrijving (contract met de bron) kan fungeren.
'------------------------------------------------------------------------------
'How to Debug PDes VBScripts 
'[HKEY_CURRENT_USER\Software\Microsoft\Windows Script\Settings] "JITDebug"=1 
'This sets the JITDebug (just-in-time debugging) key in your registry to 1 (by default it is set to 0, or disabled):
'http://www.pfcguide.com/_newsgroups/msgbody.asp?id=%3C49cd3b3b%40forums-1-dub%3E&no=28128&group=sybase.public.powerdesigner.general
'In the VBScript code itself put the statement "Stop" where you want
'the script to stop and launch VisualStudio. This will then launch VS at this
'statement and you can use the VS debugger to set breakpoints, watches,
'etc. to step through.
'------------------------------------------------------------------------------
'@ECHO OFF
'goto end_remarks
'*************************************************************************************
'* 
'*    File Type   :- Windows Batch File
'*  
'*    Purpose     :- Netwerk verbinding maken. 
'*                   alternatief voor shortcuts  %windir%\explorer.exe /e, "Q:\VHPROW62\CICT_GA\BIenD"
'*    Description :- Windows kan u helpen bij het maken van een verbinding met een gedeelde netwerkmap
'*                   en het toewijzen van een stationsletter zodat u gemakkelijk via Deze computer
'*                   toegang tot die map hebt.  
'                    Handig om op nieuw verbinding te maken bij aanmelden.
'*                   C:\Documents and Settings\boscp08\Menu Start\Programma's
'*                              
'*    this is just a launcher. you can do it manually
'*                    voorbeeld \\server\share
'*                    net use v: \\192.168.1.36\XPUT <pwd> /user:<user>
'*                    net use v: /d
'*                    net use v: \\192.168.1.36\XPUT 0lifant /user:peter
'*    
'*
'*    Modified                Date            Description
'*    --------                --------        -------------------------------------------------
'*    Peter Bosch             18.05.2011      Initial version.
'*
'**************************************************************************************
':end_remarks
'
'rem Get the datetime in a format that can go in a filename.
'set _my_datetime=%date%_%time%
'set _my_datetime=%_my_datetime: =_%
'set _my_datetime=%_my_datetime::=%
'set _my_datetime=%_my_datetime:/=_%
'set _my_datetime=%_my_datetime:,=_%
'rem now you can use the timestamp by in a new text file name
'echo N:\mappinginfo-%_my_datetime%.txt
'
'REM #################################################################################
'REM  start 
'REM  Geef de stationsletter op voor de verbinding en de map waarmee u verbinding wilt maken.
'REM #################################################################################
'
'
'set bron=DAN_SWD
'set snapshot=SWD_2015-07-23-1800_subversion
'
'
'echo bron     =  %bron%       >>n:\mappinginfo-%_my_datetime%.txt
'echo snapshot =  %snapshot%   >>n:\mappinginfo-%_my_datetime%.txt
'echo release  =  %release%    >>n:\mappinginfo-%_my_datetime%.txt
'echo domein   =  %domein%     >>n:\mappinginfo-%_my_datetime%.txt
'
'
'Rem  W: werkmap schijf
'
'REM Test if drive exists
'
'IF EXISTS W:\NUL GOTO Unmap
'
'GOTO Continue
'
':Unmap
'NET USE W: /DELETE   >>N:\mappinginfo-%_my_datetime%.txt
'
':Continue
'net use w: \\belastingdienst\edf\VHPROW62\CICT_GA\BIenD"\BDS 3387 BI&D EDW\Werkmappen\Ontwerpteam EDW\%bron%\werkmap_peter\%snapshot%"  >>N:\mappinginfo-%_my_datetime%.txt
'net use b: \\belastingdienst\edf\VHPROW62\CICT_GA\BIenD"\BDS 3387 BI&D EDW
'
'echo  '---------------'%date%_%time%   >>N:\mappinginfo-%_my_datetime%.txt
'
'EXIT
'
		
		
		
