'******************************************************************************
'* File:     detecteer_wijzigingen.vbs v20140702
'* Purpose:  diff
'* Title:    detecteer_wijzigingen
'* Category: ontwerpscript
'* Identificatie   : BI&D_EDW_MTHV\1130 Hulpmiddelen\3000 Scripts\3200 Ontwerp
'Datum    | Auteur                          | Wijziging
'20140702 | cc.wust@belastingdienst.nl      |
'???????? | nj.essenstam@belastingdients.nl |
'???????? | p.bosch@belastingdienst.nl      | 
'20151106 | r.michel@belastingdienst.nl     | Rapportage van JUNK, DROP en ARCH stereotypes verduidelijkt
'20170406 | p.bosch@belastingdienst.nl      | column.unit toegevoegd
'20170406 | p.bosch@belastingdienst.nl      | 
'******************************************************************************


'********** instructies **********

'1. start powerdesigner zonder daarbij een bestand te openen
'2. open met ctrl+shift+x het edit/run script window en laad daarin dit script
'3. definieer de powerdesigner bestanden die met elkaar vergeleken moeten worden
'   door het aanpassen van de parameters par_file1 en par_file2; gedetecteerd
'   worden de wijzigingen in par_file2 t.o.v. par_file1
'4. definieer het tekstbestand waarin de resultaten moeten worden weggeschreven
'   (op basis van append) door het aanpassen van parameter par_outfile
'5. run het script met f5

'********** parameters **********

'drive = "w:\"
'par_file1   = "02_LGM_STI_SOUTHWIND.pdm"
'par_file2   = "02_LGM_STI_SOUTHWIND_Just_a_Demo.pdm"

drive = "s:\"

par_file1   = "ONT_FGM_DOU_CDW_TBB3.pdm"
par_file2   = "DBA_FGM_DOU_CDW_TBB.pdm"


par_outfile = "detecteer_wijzigingen_"&par_file1&"_vs_"&par_file2

par_text_output  = true    
'par_excel_output = true  'Vooral bij veel wijzigingen is de leesbaarheid veel beter.


'********** parameters **********
par_alternating_colors            = true
par_insert_empty_lines            = false
par_sort_referencejoins_on_column = false
par_underline_primary_keys        = true
par_gen_hide_worksheet            = true      'tabbladen verbergen
   
'>>> excel definitions  excel rows and columns are numbered 1, 2, 3, ...
par_outfile_xlsx  = drive &replace (par_outfile , ".pdm", "" )&".xlsx"
dim excel_obj
dim matrix()
Dim words
excel_colorindex_nocolor = xlColorIndexNone'http://dmcritchie.mvps.org/excel/colors.htm
 excel_colorindex_black      = 1
 excel_colorindex_white      = 2
 excel_colorindex_red        = 3
 excel_colorindex_blue       = 5
 excel_colorindex_yellow     = 6
 excel_colorindex_magenta    = 7
 excel_colorindex_cyan       = 8
 excel_colorindex_green      = 10
 excel_colorindex_gray       = 15    
 excel_colorindex_bluegray   = 47
 excel_colorindex_source =excel_colorindex_yellow 
 excel_colorindex_target =excel_colorindex_bluegray 
 excel_colorindex_font_source = excel_colorindex_black
 excel_colorindex_font_target = excel_colorindex_white
 excel_colorindex_header = excel_colorindex_blue 
 excel_colorindex_avro = excel_colorindex_blue'
 excel_colorindex_font_header = excel_colorindex_white 
 excel_colorindex_1= excel_colorindex_black 
 excel_colorindex_2= excel_colorindex_blue
 zebra = excel_colorindex_1
 
'------------------------------------------------------------------------------ 
vbstartdate = date  
vbstarttime = time  
nu= year(date) & month (date) & day(date) & "_"  & hour(time)& "u" & minute(time) & "_"  & second(time) 
   
if par_text_output = true then 
 par_outfile_txt   = drive & replace (par_outfile , ".pdm", "" )&nu&".txt"
set system = createobject("scripting.filesystemobject")
set outfile = system.opentextfile(par_outfile_txt , 8, 1)

	outfile.write  "> VB Script started on " & date & " at " & time & vbnewline
  outfile.write  "wijzigingen in " & par_file2 & " t.o.v. " & par_file1 & ":" & vbnewline
end if  

set m1 = openmodel(par_file1, omf_dontopenview or omf_hidden)
set m2 = openmodel(par_file2, omf_dontopenview or omf_hidden)

geen_wijzigingen_gedetecteerd = true

if par_excel_output = true then
'>>> create a new excel proces (file)
  set dummy = excel_new_file(false, 1) '1 onzichtbare werkbladen
end if  


'****************************************************************************
'FYSICAL OPTIONS 

for each t1 in m1.tables
   if not t1.isshortcut then
      match = false
      for each t2 in m2.tables
         if not t1.isshortcut then
           
           'if t.PhysicalOptions = t2.PhysicalOptions 
            
        if t1.code  = t2.code then
        
         
         Classificatie1 = LEFT(t1.physicaloptions ,24) ' IN <DB2DBCDW09>.CDWTS240
         Classificatie2 = LEFT(t2.physicaloptions ,24) ' IN <DB2DBCDW09>.CDWTS240
        
        
         if Classificatie1 = Classificatie2  then
         
               match = true
               exit for
             end if  
            end if
         end if
         
         
        ';  output t1.physicaloptions
          '     msgbox Classificatie1&"->"&Classificatie1
      next
     
      if match = false then
         geen_wijzigingen_gedetecteerd = false
         
         output t1.physicaloptions
         '--------------------
          if par_text_output = true then 
         	 outfile.write     "- PhysicalOptions gewijzigd :"&t1.code&""""&Classificatie1&"""->"""&Classificatie2& """" & vbnewline
          end if
         
         
         ' if par_excel_output = true then 
         ' 	detect_rowid = detect_rowid + 1
         '   set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, "PhysicalOptions gewijzi" )  
         '   set dummy = excel_set_cell_value_3d(1, detect_rowid, 2, t2.physicaloptions )                       
         ' end if  
          '--------------------
         
      end if
   end if
next


'****************************************************************************

'Indexen

'databases

'storages
     
'******************************************************************* CLEMENS DEEL
 '>>> sheet 1: wijzigingen
     'initialize the matrix
      
      detect_rowid  = 2   'worksheet 2 teller voor wijzigin

for each t2 in m2.tables
   if not t2.isshortcut then
      match = false
      for each t1 in m1.tables
         if not t1.isshortcut then
            if t1.code = t2.code then
               match = true
               exit for
            end if
         end if
      next
      if match = false then
         geen_wijzigingen_gedetecteerd = false
         
         '--------------------
          if par_text_output = true then 
         	 outfile.write  "- entiteit toegevoegd: """ & t2.code & """" & vbnewline
          end if
         
          if par_excel_output = true then 
          	detect_rowid = detect_rowid + 1
            set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, "entiteit toegevoegd" )  
            set dummy = excel_set_cell_value_3d(1, detect_rowid, 2, t2.code )                       
          end if  
          '--------------------
         
      end if
   end if
next


for each t1 in m1.tables
   if not t1.isshortcut then
      match = false
      for each t2 in m2.tables
         if not t2.isshortcut then
            if t1.code = t2.code then
               match = true
               exit for
            end if
         end if
      next
      if match = false then
         geen_wijzigingen_gedetecteerd = false
         
         '--------------------
          if par_text_output = true then 
         	 outfile.write  "- entiteit verwijderd: """ & t1.code & """" & vbnewline
          end if
         
          if par_excel_output = true then 
          	detect_rowid = detect_rowid + 1
            set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, "entiteit verwijderd" )  
            set dummy = excel_set_cell_value_3d(1, detect_rowid, 2, t1.code )                       
          end if  
          '--------------------
         
      end if
   end if
next







for each t1 in m1.tables
   if not t1.isshortcut then
      for each t2 in m2.tables
         if not t2.isshortcut then
            if t1.code = t2.code then              
               andere_volgorde = false
               i = -1
               for each c1 in t1.columns
                  for j = 0 to t2.columns.count - 1
                     set c2 = t2.columns.item(j)
                     if c1.code = c2.code then
                        if i < j then
                           i = j
                        else
                           andere_volgorde = true
                        end if
                        exit for
                     end if
                  next
                  if andere_volgorde = true then exit for
               next
               if andere_volgorde = true then
                  geen_wijzigingen_gedetecteerd = false
                  Classificatie = JUNK_test(t1.stereotype)
				  
                  if par_text_output = true then 
                  	outfile.write  "- " & Classificatie & "entiteit gewijzigd: """ & t1.code & """ (attribuutvolgorde)" & vbnewline   
                  end if	
                  
                   if par_excel_output = true then 
                   	detect_rowid = detect_rowid + 1
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, Classificatie & "entiteit gewijzigde" )  
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 2, t1.code )
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 4, "attribuutvolgorde") 
                   end if 
                        
               end if
               if t1.stereotype <> t2.stereotype then
                  geen_wijzigingen_gedetecteerd = false
                  Classificatie = JUNK_test2(t1.stereotype,t2.stereotype)
                  
                   if par_text_output = true then 
                  	 outfile.write  "- " & Classificatie & "entiteit gewijzigd: """ & t1.code & """ (stereotype: """ & t1.stereotype & """ -> """ & t2.stereotype & """)" & vbnewline
                   end if	
                  
                   if par_excel_output = true then
                    detect_rowid = detect_rowid + 1
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, Classificatie & "entiteit gewijzigd" ) 
                    set dummy = excel_set_cell_value_3d(1, detect_rowid,2, t1.code )
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 4, "stereotype: """ & t1.stereotype & """ -> """ & t2.stereotype  ) 
                   end if
               end if
               exit for
            end if
         end if
      next
   end if
next

for each t1 in m1.tables
   if not t1.isshortcut then
      for each t2 in m2.tables
         if not t2.isshortcut then
            if t1.code = t2.code then
               for each c1 in t1.columns
                  match = false
                  for each c2 in t2.columns
                     if c1.code = c2.code then
                        match = true
                        exit for
                     end if            
                  next
                  if match = false then
                     geen_wijzigingen_gedetecteerd = false
				        	 Classificatie = JUNK_test(c1.stereotype)
                  '--------------------
                   if par_text_output = true then 
                      	outfile.write  "- " & Classificatie & "attribuut verwijderd: """ & t1.code & "." & c1.code & """" & vbnewline
                   end if	
                  if par_excel_output =  true then 
                  	detect_rowid = detect_rowid + 1
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, Classificatie & "attribuut verwijderd"     )  
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 2,  t1.code   ) 
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 3,  c1.code  ) 
                  end if   
                  '-----------------------
                  end if
               next
               for each c2 in t2.columns
                  match = false
                  for each c1 in t1.columns
                     if c1.code = c2.code then
                        match = true
                        exit for
                     end if            
                  next
                  if match = false then
                     geen_wijzigingen_gedetecteerd = false
					 Classificatie = JUNK_test(c2.stereotype)
                  
                  '-----------------------
                   if par_text_output = true then 
                     	outfile.write  "- " & Classificatie & "attribuut toegevoegd: """ & t2.code & "." & c2.code & """" & vbnewline
                   end if 	
                     
                  if par_excel_output = true then 
                  	detect_rowid = detect_rowid + 1
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, Classificatie & "attribuut toegevoegd" )  
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 2,  t2.code   ) 
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 3,  c2.code   )
                  end if  
                  '-----------------------  xxxxxxxxxxxxxxx
                     
                     
                  end if
               next                              
               exit for
            end if
         end if
      next
   end if
next

for each t1 in m1.tables
   if not t1.isshortcut then
      for each t2 in m2.tables
         if not t2.isshortcut then
            if t1.code = t2.code then
               for each c1 in t1.columns
                  for each c2 in t2.columns
                     if c1.code = c2.code then
                        if c1.datatype <> c2.datatype  then
                           geen_wijzigingen_gedetecteerd = false
					            	   Classificatie = JUNK_test(c1.stereotype)
                           
                           '----------------------- 
                            if par_text_output = true then 
                            	outfile.write  "- " & Classificatie & "attribuut gewijzigd: """ & t1.code & "." & c1.code & """ (datatype: """  & c1.datatype  & """ -> """ & c2.datatype  & """)" & vbnewline
                            end if	
                           
                           if par_excel_output = true then 
                           	detect_rowid = detect_rowid + 1
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, Classificatie & "attribuut gewijzigd" )  
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 2,  t1.code   ) 
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 3,  c1.code  ) 
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 4, "datatype:"""  &   c1.datatype  & """ -> """ & c2.datatype& """")
                           end if
                           
                           '-----------------------  
                           
                        end if
                        
                        
                         if c1.unit <> c2.unit  then   'v6 20170406
                           geen_wijzigingen_gedetecteerd = false
					            	   Classificatie = JUNK_test(c1.stereotype)
                           
                           '----------------------- 
                            if par_text_output = true then 
                            	outfile.write  "- " & Classificatie & "attribuut gewijzigd: """ & t1.code & "." & c1.code & """ (unit: """  & c1.unit  & """ -> """ & c2.unit  & """)" & vbnewline
                            end if	
                           
                           if par_excel_output = true then 
                           	detect_rowid = detect_rowid + 1
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, Classificatie & "attribuut gewijzigd" )  
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 2,  t1.code   ) 
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 3,  c1.code  ) 
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 4, "unit:"""  &   c1.unit  & """ -> """ & c2.unit& """")
                           end if
                           
                           '-----------------------  
                           
                        end if
                        
                        
                        if c1.mandatory <> c2.mandatory then
                           geen_wijzigingen_gedetecteerd = false
						             Classificatie = JUNK_test(c1.stereotype)
                            
                            '----------------------- 
                            if par_text_output = true then 
                            	outfile.write  "- " & Classificatie & "attribuut gewijzigd: """ & t1.code & "." & c1.code & """ (mandatory: """ & c1.mandatory & """ -> """ & c2.mandatory & """)" & vbnewline
                            end if
                            
                           if par_excel_output = true then 
                           	detect_rowid = detect_rowid + 1
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, Classificatie & "attribuut gewijzigd" )  
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 2,  t1.code   ) 
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 3,  c1.code  )
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 4, "mandatory:"""  &   c1.mandatory  & """ -> """ & c2.mandatory& """")
                           end if 
                           '----------------------- 
                        
                        end if
                        if c1.primary <> c2.primary then
                           geen_wijzigingen_gedetecteerd = false
						   Classificatie = JUNK_test(c1.stereotype)
                           
                           '----------------------- 
                           if par_text_output = true then 
                           	outfile.write  "- " & Classificatie & "attribuut gewijzigd: """ & t1.code & "." & c1.code & """ (primary: """   & c1.primary   & """ -> """ & c2.primary   & """)" & vbnewline
                           end if	
                           if par_excel_output = true then 
                           	detect_rowid = detect_rowid + 1
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, Classificatie & "attribuut gewijzigd" )  
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 2,  t1.code   ) 
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 3,  c1.code  )
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 4, "primary:"""  &   c1.primary  & """ -> """ & c2.primary& """")
                           end if 
                           '-----------------------  
                        end if
                        if c1.stereotype <> c2.stereotype then
                           geen_wijzigingen_gedetecteerd = false
						   Classificatie = JUNK_test2(c1.stereotype,c2.stereotype)
                           
                           '----------------------- 
                           if par_text_output = true then 
                           	 outfile.write  "- " & Classificatie & "attribuut gewijzigd: """ & t1.code & "." & c1.code & """ (stereotype: """ & c1.stereotype & """ -> """ & c2.stereotype & """)" & vbnewline
                           end if	 
                        
                           if par_excel_output = true then 
                           	detect_rowid = detect_rowid + 1
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, Classificatie & "attribuut gewijzigd" )  
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 2,  t1.code   ) 
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 3,  c1.code  )
                            set dummy = excel_set_cell_value_3d(1, detect_rowid, 4, "stereotype:"""  &   c1.stereotype  & """ -> """ & c2.stereotype& """")
                           end if
                           '-----------------------  
                        
                        end if
                        exit for
                     end if            
                  next
               next
               exit for
            end if
         end if
      next
   end if
next

for each r1 in m1.references
   if not r1.isshortcut then
      for each j1 in r1.joins
         match = false
         for each r2 in m2.references
            if not r2.isshortcut then
               for each j2 in r2.joins
                  if ((r1.childtable.code = r2.childtable.code) and (j1.childtablecolumn.code = j2.childtablecolumn.code) and (r1.parenttable.code = r2.parenttable.code) and (j1.parenttablecolumn.code = j2.parenttablecolumn.code)) then
                     match = true
                     exit for
                  end if
               next
            end if
            if match = true then exit for
         next
         if match = false then
            geen_wijzigingen_gedetecteerd = false
            Classificatie = JUNK_test(j1.stereotype)
			
            '----------------------- 
             if par_text_output = true then 
             	outfile.write  "- " & Classificatie & "join verwijderd: [child """ & r1.childtable.code & "." & j1.childtablecolumn.code & """, parent """ & r1.parenttable.code & "." & j1.parenttablecolumn.code & """]" & vbnewline
            end if
            if par_excel_output = true then 
            	detect_rowid = detect_rowid + 1
              set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, Classificatie & "join verwijderd" )  
              set dummy = excel_set_cell_value_3d(1, detect_rowid, 2,  r1.childtable.code  ) 
              set dummy = excel_set_cell_value_3d(1, detect_rowid, 3,  j1.childtablecolumn.code ) 
              set dummy = excel_set_cell_value_3d(1, detect_rowid, 4, "[child """ & r1.childtable.code & "." & j1.childtablecolumn.code & """, parent """ & r1.parenttable.code & "." & j1.parenttablecolumn.code& """]")
            end if 
            '----------------------- 
              
         end if
      next
   end if
next

for each r2 in m2.references
   if not r2.isshortcut then
      for each j2 in r2.joins
         match = false
         for each r1 in m1.references
            if not r1.isshortcut then
               for each j1 in r1.joins
                  if ((r1.childtable.code = r2.childtable.code) and (j1.childtablecolumn.code = j2.childtablecolumn.code) and (r1.parenttable.code = r2.parenttable.code) and (j1.parenttablecolumn.code = j2.parenttablecolumn.code)) then
                     match = true
                     exit for
                  end if
               next
            end if
            if match = true then exit for
         next
         if match = false then
            geen_wijzigingen_gedetecteerd = false
            Classificatie = JUNK_test(j2.stereotype)

            '----------------------- 
             if par_text_output = true then 
             	 outfile.write  "- " & Classificatie & "join toegevoegd: [child """ & r2.childtable.code & "." & j2.childtablecolumn.code & """, parent """ & r2.parenttable.code & "." & j2.parenttablecolumn.code & """]" & vbnewline
             end if
            
            if par_excel_output = true then
            	 detect_rowid = detect_rowid + 1
               set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, Classificatie & "join toegevoegd" ) 
               set dummy = excel_set_cell_value_3d(1, detect_rowid, 2,  r2.childtable.code  ) 
               set dummy = excel_set_cell_value_3d(1, detect_rowid, 3,  j2.childtablecolumn.code ) 
               set dummy = excel_set_cell_value_3d(1, detect_rowid, 4, "child " & r2.childtable.code & "." & j2.childtablecolumn.code & """, parent """ & r2.parenttable.code & "." & j2.parenttablecolumn.code ) 
            end if
            '----------------------- 
               
         end if
      next
   end if
next

for each r1 in m1.references
   if not r1.isshortcut then
      for each r2 in m2.references
         if not r2.isshortcut then
            if ((r1.childtable.code = r2.childtable.code) and (r1.parenttable.code = r2.parenttable.code) and (r1.joins.count = r2.joins.count)) then
               match = true
               for i = 0 to r1.joins.count - 1
                  set j1 = r1.joins.item(i)
                  set j2 = r2.joins.item(i)
                  if ((j1.childtablecolumn.code <> j2.childtablecolumn.code) or (j1.parenttablecolumn.code <> j2.parenttablecolumn.code)) then
                     match = false
                     exit for
                  end if
               next
               if match = true then
                  if r1.stereotype <> r2.stereotype then
                     geen_wijzigingen_gedetecteerd = false
					 Classificatie = JUNK_test(r1.stereotype)
                   
                   '----------------------- 
                      if par_text_output = true then 
                      	outfile.write  "- " & Classificatie & "reference gewijzigd: [child """ & r1.childtable.code & """, parent """ & r1.parenttable.code & """] (stereotype: """ & r1.stereotype & """ -> """ & r2.stereotype & """)" & vbnewline
                      end if	
                      
                   if par_excel_output = true then 
                   	detect_rowid = detect_rowid + 1
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, Classificatie & "reference gewijzigd" )  
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 2,  r1.childtable.code  ) 
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 3,  r1.parenttable.code)
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 4, "[child """ & r1.childtable.code & """, parent """ & r1.parenttable.code & """]  (stereotype: """ & r1.stereotype & """ -> """ & r2.stereotype )
                   end if
                 '----------------------- 
                  
                  end if
               end if
            end if
         end if
      next
   end if
next


for each t1 in m1.views
   if not t1.isshortcut then
      match = false
      for each t2 in m2.views
         if not t2.isshortcut then
            if t1.code = t2.code then
               match = true
               exit for
            end if
         end if
      next
      if match = false then
         geen_wijzigingen_gedetecteerd = false
         Classificatie = JUNK_test(t1.stereotype)
		 
         '--------------------
          if par_text_output = true then 
         	 outfile.write  "- " & Classificatie & "view verwijderd: """ & t1.code & """" & vbnewline
          end if
         
          if par_excel_output = true then 
          	detect_rowid = detect_rowid + 1
            set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, Classificatie & "view verwijderd" )  
            set dummy = excel_set_cell_value_3d(1, detect_rowid, 2, t1.code )                       
          end if  
          '--------------------
         
      end if
   end if
next

for each t2 in m2.views
   if not t2.isshortcut then
      match = false
      for each t1 in m1.views
         if not t1.isshortcut then
            if t1.code = t2.code then
               match = true
               exit for
            end if
         end if
      next
      if match = false then
         geen_wijzigingen_gedetecteerd = false
         Classificatie = JUNK_test(t2.stereotype)

		 if par_text_output = true then 
          	outfile.write  "- " & Classificatie & "view toegevoegd: """ & t2.code & """" & vbnewline
          end if 	
         
         if par_excel_output = true then 
         	detect_rowid = detect_rowid + 1
          set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, Classificatie & "view toegevoegd" )  
          set dummy = excel_set_cell_value_3d(1, detect_rowid, 2, t2.code )    
         end if 
         
         
      end if
   end if
next


for each t1 in m1.views
   if not t1.isshortcut then
      for each t2 in m2.views
         if not t2.isshortcut then
            if t1.code = t2.code then
               for each c1 in t1.columns
                  match = false
                  for each c2 in t2.columns
                     if c1.code = c2.code then
                        match = true
                        exit for
                     end if            
                  next
                  if match = false then
                     geen_wijzigingen_gedetecteerd = false
			         Classificatie = JUNK_test(t1.stereotype)

                  '--------------------
                   if par_text_output = true then 
                      	outfile.write  "- " & Classificatie & "view attribuut verwijderd: """ & t1.code & "." & c1.code & """" & vbnewline
                   end if	
                  if par_excel_output =  true then 
                  	detect_rowid = detect_rowid + 1
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, Classificatie & "view attribuut verwijderd"     )  
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 2,  t1.code   ) 
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 3,  c1.code  ) 
                  end if   
                  '-----------------------
                  end if
               next
               for each c2 in t2.columns
                  match = false
                  for each c1 in t1.columns
                     if c1.code = c2.code then
                        match = true
                        exit for
                     end if            
                  next
                  if match = false then
                     geen_wijzigingen_gedetecteerd = false
   			         Classificatie = JUNK_test(c2.stereotype)

                  '-----------------------
                   if par_text_output = true then 
                     	outfile.write  "- " & Classificatie & "view attribuut toegevoegd: """ & t2.code & "." & c2.code & """" & vbnewline
                   end if 	
                     
                  if par_excel_output = true then 
                  	detect_rowid = detect_rowid + 1
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 1,  Classificatie & "view attribuut toegevoegd" )  
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 2,  t2.code   ) 
                    set dummy = excel_set_cell_value_3d(1, detect_rowid, 3,  c2.code   )
                  end if  
                  '-----------------------  
                     
                     
                  end if
               next                              
               exit for
            end if
         end if
      next
   end if
next







if geen_wijzigingen_gedetecteerd = true then
   
   output "resultaat "&  par_file1&"_vs_"&par_file2
   output "Geen wijzigingen gedetecteerd ! " 
   output ""
       
   '----------------------- 
    if par_text_output = true then 
    	 outfile.write  "- geen wijzigingen gedetecteerd" & vbnewline
    end if	 
    
    if par_excel_output = true then
    	 detect_rowid = detect_rowid + 1
       set dummy = excel_set_cell_value_3d(1, detect_rowid, 1, "geen wijzigingen gedetecteerd" )  
    end if    
    '----------------------- 
    
end if


'klaar met processing 




if par_text_output = true then
   outfile.write  "> VB Script terminated on " & date & " at " & time & vbnewline
   outfile.write  vbnewline
   outfile.close
end if   

 if par_excel_output = true then
 set dummy = excel_select_worksheet(1)
    set dummy = excel_set_worksheet_name(1, "detecteer_wijzigingen", true)
    set dummy = excel_set_worksheet_tabcolor(1,  excel_colorindex_red   ) 
    
        'set headers      
        set dummy = excel_set_cell_value(2, 1  , "Diff on " & date & " at " & time   )'  reference.annotation                1 
        set dummy = excel_set_cell_value(2, 2  , "table"                        )'  reference.annotation                2 
        set dummy = excel_set_cell_value(2, 3  , "column"                       )'  reference.annotation                3 
        set dummy = excel_set_cell_value(2, 3  , "details"                      )'  reference.annotation                3 
        
        'coloring  
        set dummy = excel_set_font_columns(1,12)                                                                                       
        set dummy = excel_set_cells_background_color(1, 1, 2, 4, excel_colorindex_target)                                            
        set dummy = excel_set_cells_font_color(1, 1, 2, 4, excel_colorindex_font_target)   'row col                                            
       
       'freeze
       set dummy = excel_freeze_row(3)                             
       set dummy = excel_autofit_columns(1, 4)            
       set dummy = excel_set_cursor(1, 1)
       set dummy = excel_set_autofilter(2, 1, 2, 4)  ' excel_obj.range("A2:D2").AutoFilter

       set dummy = excel_set_cell_value (1,1,"wijzigingen in " & par_file2 & " t.o.v. " & par_file1 & ":")
               
       set dummy = excel_save_as_file(par_outfile_xlsx)
       set dummy = excel_close_file()
  
end if 



if par_excel_output = true then
	output "--"
   output "output is weggeschreven naar " & par_outfile_xlsx
   output ""
end if 

if par_text_output = true then 
    output "--"
    output "output is weggeschreven naar " & par_outfile_txt
    output ""
end if 


output "hope the run was okay!"
msgbox "klaar met processing "



'------------------------------------------------------------------------------
'#Powerdesigner is te beschouwen als een [ObjectOriented XML database] 
'#De OO getters setters staan hieronder
'------------------------------------------------

'>>> functions that are called by other functions
'# Structured programming:
'# Entire program logic modularized in functions.
' ------------------------------
 
'********** excel functions **********
' many thanks to cc.wust@belastingdienst.nl
'>>> functions that are called by other functions
'excel rows and columns are numbered 1, 2, 3, ...
'>>> definitions
 
'>>> functions that are called by other functions
 
function excel_get_row(excel_row_nr)
   excel_get_row = cstr(excel_row_nr)
end function
 
function excel_get_rows(excel_row_nr1, excel_row_nr2)
   excel_get_rows = excel_get_row(excel_row_nr1) & ":" & excel_get_row(excel_row_nr2)
end function
 
function excel_get_column(excel_column_nr)
   letter1 = (excel_column_nr - 1) \ 26
   letter2 = ((excel_column_nr - 1) mod 26) + 1
   if letter1 = 0 then
      excel_get_column = chr(64 + letter2)
   else
      excel_get_column = chr(64 + letter1) & chr(64 + letter2)
   end if
end function
 
function excel_get_columns(excel_column_nr1, excel_column_nr2)
   excel_get_columns = excel_get_column(excel_column_nr1) & ":" & excel_get_column(excel_column_nr2)
end function
 
function excel_get_cell(excel_row_nr, excel_column_nr)
   excel_get_cell = excel_get_column(excel_column_nr) & excel_get_row(excel_row_nr)
end function
 
function excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)
   excel_get_cells = excel_get_column(excel_column_nr1) & excel_get_row(excel_row_nr1) & ":" & excel_get_column(excel_column_nr2) & excel_get_row(excel_row_nr2)
end function
 
'>>> functions to set or read the value of a cell
 
function excel_set_cell_value(excel_row_nr, excel_column_nr, value)
   excel_obj.range(excel_get_cell(excel_row_nr, excel_column_nr)).value = value
   set excel_set_cell_value = nothing
end function

function excel_set_cell_value_3d(excel_worksheet_nr, excel_row_nr, excel_column_nr, value)
   set dummy = excel_select_worksheet(excel_worksheet_nr)
   excel_obj.range(excel_get_cell(excel_row_nr, excel_column_nr)).value = value
   set excel_set_cell_value_3d = nothing
end function
 
 
function excel_set_cell_comment(excel_row_nr, excel_column_nr, value)
 
set dummy = excel_set_cursor(excel_row_nr, excel_column_nr)
   With excel_obj.range(excel_get_cell(excel_row_nr, excel_column_nr))
    .AddComment  value
     
     With .Comment.Shape
        .Height = 200
        .Width = 100
    End With
'    .Comment.Visible = false
 
End With 
 
  
   set excel_set_cell_comment = nothing
end function
 
 
function excel_get_cell_value(excel_row_nr, excel_column_nr)
   excel_get_cell_value = excel_obj.range(excel_get_cell(excel_row_nr, excel_column_nr)).value
end function
 
'>>> i/o functions
function excel_new_file(visible, nr_sheets)
   set excel_obj = createobject("excel.application")
   excel_obj.visible = visible
   excel_obj.sheetsinnewworkbook = nr_sheets
   excel_obj.workbooks.add
   set excel_new_file = nothing
end function
 
function excel_load_file(visible, filename)
   set excel_obj = createobject("excel.application")
   excel_obj.visible = visible
   excel_obj.workbooks.open(filename)
   excel_obj.worksheets(1).select
   set excel_load_file = nothing
end function
 
function excel_save_file()
   excel_obj.activeworkbook.save
   set excel_save_file = nothing
end function
 
function excel_save_as_file(filename)
   excel_obj.activeworkbook.saveas(filename)
 
   set excel_save_as_file = nothing
end function
 
function excel_close_file()
   excel_obj.activeworkbook.close
   set excel_close_file = nothing
end function
 
'>>> worksheet functions
 
function excel_select_worksheet(worksheet_nr)
   excel_obj.worksheets(worksheet_nr).select
   set excel_select_worksheet = nothing
end function
 
function excel_set_worksheet_name(worksheet_nr, worksheet_name, visible )
   excel_obj.worksheets(worksheet_nr).name = worksheet_name
   excel_obj.worksheets(worksheet_nr).visible = visible
   
        set excel_set_worksheet_name = nothing
end function
 
function excel_get_worksheet_name(worksheet_nr)
   excel_get_worksheet_name = excel_obj.worksheets(worksheet_nr).name
end function

function excel_set_worksheet_tabcolor(worksheet_nr, tabkleur )
   excel_obj.worksheets(worksheet_nr).tab.colorindex  = tabkleur
   set excel_set_worksheet_tabcolor = nothing
end function

 
'>>> layout functions
 
function excel_set_cursor(excel_row_nr, excel_column_nr)
   excel_obj.range(excel_get_cell(excel_row_nr, excel_column_nr)).select
   set excel_set_cursor = nothing
end function

function  excel_set_autofilter(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)
   excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).AutoFilter
       set excel_set_autofilter = nothing
end function


function excel_merge_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)
   excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).select
   excel_obj.selection.merge
   set excel_merge_cells = nothing
end function
 
function excel_set_zoom(zoom_percentage)
   excel_obj.activewindow.zoom = zoom_percentage
   set excel_set_zoom = nothing
end function
 
function excel_autofit_columns(excel_column_nr1, excel_column_nr2)
   excel_obj.columns(excel_get_columns(excel_column_nr1, excel_column_nr2)).autofit
   set excel_autofit_columns = nothing
end function
 
function excel_freeze_row(excel_row_nr)
   excel_obj.rows(excel_get_rows(excel_row_nr, excel_row_nr)).select
   excel_obj.activewindow.freezepanes = true
   set excel_freeze_row = nothing
end function
 
function excel_freeze_column(excel_column_nr)
   excel_obj.columns(excel_get_columns(excel_column_nr, excel_column_nr)).select
   excel_obj.activewindow.freezepanes = true
   set excel_freeze_column = nothing
end function
 
function excel_freeze_cell(excel_row_nr, excel_column_nr)
   excel_obj.range(excel_get_cell(excel_row_nr, excel_column_nr)).select
   excel_obj.activewindow.freezepanes = true
   set excel_freeze_cell = nothing
end function
 
function excel_set_cells_background_color(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2, excel_colorindex)
   excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).interior.colorindex = excel_colorindex
   set excel_set_cells_background_color = nothing
end function
 
function excel_set_cells_border_color(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2, excel_colorindex)
   excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).select
   excel_obj.selection.borders.linestyle = xlContinuous
    excel_obj.selection.borders.colorindex = excel_colorindex
   set excel_set_cells_border_color = nothing
end function


function excel_set_font_columns(excel_column_nr1, excel_column_nr2)
' excel_obj.columns(excel_get_columns(excel_column_nr1, excel_column_nr2)).font.Name = "Arial"
 excel_obj.columns(excel_get_columns(excel_column_nr1, excel_column_nr2)).font.Name = "Lucida Console"
 excel_obj.columns(excel_get_columns(excel_column_nr1, excel_column_nr2)).font.size = 10
 set excel_set_font_columns = nothing
end function

 
function excel_set_cells_font_color(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2, excel_colorindex)
   excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).font.colorindex = excel_colorindex
   set excel_set_cells_font_color = nothing
end function
 
function excel_set_cells_bold(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2, activate)
   excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).font.bold = activate
   set excel_set_cells_bold = nothing
end function
 
function excel_set_cells_italic(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2, activate)
   excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).font.italic = activate
   set excel_set_cells_italic = nothing
end function
 
function excel_set_cells_underline(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2, activate)
   excel_obj.range(excel_get_cells(excel_row_nr1, excel_column_nr1, excel_row_nr2, excel_column_nr2)).font.underline = activate
   set excel_set_cells_underline = nothing
end function

function JUNK_test(Stereotype)
   Waarde = ""
   if Instr( 1, Stereotype, "JUNK", vbTextCompare ) > 0 then Waarde = "JUNK " end if
   if Instr( 1, Stereotype, "DROP", vbTextCompare ) > 0 then Waarde = "DROP " end if
   if Instr( 1, Stereotype, "ARCH", vbTextCompare ) > 0 then Waarde = "ARCH " end if
   JUNK_test = Waarde
end function

function JUNK_test2(Stereotype1, Stereotype2)
'geeft waarschuwing weg als ��n van de 2 stereotypes in de lijst valt
   Waarde1 = ""
   Waarde2 = ""
   Returnwaarde = ""
   if Instr( 1, Stereotype1, "JUNK", vbTextCompare ) > 0 then Waarde1 = "JUNK " end if
   if Instr( 1, Stereotype1, "DROP", vbTextCompare ) > 0 then Waarde1 = "DROP " end if
   if Instr( 1, Stereotype1, "ARCH", vbTextCompare ) > 0 then Waarde1 = "ARCH " end if
   if Instr( 1, Stereotype2, "JUNK", vbTextCompare ) > 0 then Waarde2 = "JUNK " end if
   if Instr( 1, Stereotype2, "DROP", vbTextCompare ) > 0 then Waarde2 = "DROP " end if
   if Instr( 1, Stereotype2, "ARCH", vbTextCompare ) > 0 then Waarde2 = "ARCH " end if
   if Waarde1 <> "" and Waarde2 = "" then Returnwaarde = "!!! Niet meer " & Waarde1 & "- " end if
   if Waarde1 = "" and Waarde2 <> "" then Returnwaarde = Waarde2 end if
   if Waarde1 <> "" and Waarde2 <> "" and Waarde1 <> Waarde2 then Returnwaarde = Waarde1 & "-> " & Waarde2& "- " end if
   JUNK_test2 = Returnwaarde
end function
