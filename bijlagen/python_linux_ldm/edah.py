from LDMHandler import LDMHandler
import sys


def parse_rel(isa):
  print("======Relationships======")
  filename = isa
  ph = LDMHandler.parse(filename)

  for pkg in LDMHandler.getPkgNodes(ph):
    pkg_attrs = LDMHandler.getPkgAttrs(pkg)

    for rel in LDMHandler.getRelNodesInPkg(pkg):
      rel_attrs = LDMHandler.getRelAttrs(rel)
      print(" R:", rel_attrs["Name"], rel_attrs["Code"], rel_attrs["Creator"])
  print("======dze end of Relationships ======")

def parse_isa(isa):
  print("======Entities======")
  filename = isa
  ph = LDMHandler.parse(filename)

  for pkg in LDMHandler.getPkgNodes(ph):
    pkg_attrs = LDMHandler.getPkgAttrs(pkg)

    for ent in LDMHandler.getEntNodesInPkg(pkg):
      ent_attrs = LDMHandler.getEntAttrs(ent)
      print(" E:", ent_attrs["ObjectID"], ent_attrs["Name"], ent_attrs["Code"], ent_attrs["Creator"])

      for atb in LDMHandler.getAtbNodesInEnt(ent):
        atb_attrs = LDMHandler.getAtbAttrs(atb)
        print("  A:", atb_attrs["Name"], atb_attrs["Code"], atb_attrs["DataType"], atb_attrs["Length"])
  print("======dze end of entities ======")



def main(isa):
  isa = sys.argv[1]
  parse_isa(isa)
  parse_rel(isa)

if __name__ == "__main__":
  main(sys.argv[1])  # ldm.xml
     
   
