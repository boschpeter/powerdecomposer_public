#!/usr/bin/python3
# -*- coding: utf-8 -*- #

from PDMHandler import PDMHandler
if __name__ == '__main__' :
  import sys
  #reload(sys)
  #sys.setdefaultencoding("utf-8")
  
  filename = sys.argv[1]
  ph = PDMHandler.parse(filename)
  for pkg in PDMHandler.getPkgNodes(ph):
    pkg_attrs = PDMHandler.getPkgAttrs(pkg)
    print ("P:", pkg_attrs["Name"],pkg_attrs["Code"],pkg_attrs["Creator"])
    