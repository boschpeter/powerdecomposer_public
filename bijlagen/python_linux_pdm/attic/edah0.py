# https://stackoverflow.com/questions/16183222/xml-file-as-command-line-argument-and-parse-in-python
import sys
import xml.etree.ElementTree as ET

if __name__ == '__main__':
    #print 'args:', sys.argv
    #print 'last arg:', sys.argv[-1]
    if len(sys.argv) <= 1:
        sys.exit()
    tree = ET.parse(sys.argv[-1])
    root = tree.getroot()
    for child in root:
        print child.tag, child.text
