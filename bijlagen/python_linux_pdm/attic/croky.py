# from xml.dom.minidom import parse
# import xml.dom.minidom
# import xml.etree.ElementTree as ET
# https://stackoverflow.com/questions/40154727/how-to-use-xmltodict-to-get-items-out-of-an-xml-file
# https://www.guru99.com/manipulating-xml-with-python.html
# https://docs.python-guide.org/scenarios/xml/
# https://dzone.com/articles/reading-an-xml-in-python
# https://www.geeksforgeeks.org/convert-json-to-dictionary-in-python/

import xmltodict
## Reading an XML file is highly simplified using the xmltodict package available in Python. Use pip to install and try it.
## Converting XML File to JSON


from pprint import pprint as pp
import os
import shutil
import sys
import pprint
import json

#pp = pprint.PrettyPrinter(indent=4)
 
def verwerk_json(doc):
 with open(doc) as json_file:
    data = json.load(json_file)
 print("Type:", type(data))

def print_isa(doc):
  print ("======xmltodict======")
  #print (doc)
  pp = pprint.PrettyPrinter(indent=10)
  pp.pprint(json.dumps(doc))
  print ("======end======")

def main(isa):
  isa = sys.argv[1]
  with open(isa) as fd:
     doc = xmltodict.parse(fd.read())
  print_isa(doc)
  #verwerk_json(doc)

if __name__ == "__main__":
    main(sys.argv[1])  # ldm.xml
   
     
   