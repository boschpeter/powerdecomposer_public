import random
import string

# printing lowercase
letters = string.ascii_lowercase
print ( ''.join(random.choice(letters) for i in range(30)) )

# printing uppercase
letters = string.ascii_uppercase
print ( ''.join(random.choice(letters) for i in range(30)) )

# printing letters
letters = string.ascii_letters
print ( ''.join(random.choice(letters) for i in range(30)) )

# printing digits
letters = string.digits
print ( ''.join(random.choice(letters) for i in range(30)) )

# printing punctuation
letters = string.punctuation
