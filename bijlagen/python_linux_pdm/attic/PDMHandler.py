#!/usr/bin/python
# -*- coding: utf-8 -*- #

from xml.dom.minidom import parse
import xml.dom.minidom

class PDMHandler(object):
  PKG_ATTR_LIST    = ["Name","Code","CreationDate","Creator","ModificationDate","Modifier"]
  TBL_ATTR_LIST    = ["Name","Code","CreationDate","Creator","ModificationDate","Modifier",
                      "PhysicalOptions"]
  COL_ATTR_LIST    = ["Name","Code","CreationDate","Creator","ModificationDate","Modifier",
                      "DataType","Length","Column.Mandatory","Comment"]
  IDX_ATTR_LIST    = ["Name","Code","CreationDate","Creator","ModificationDate","Modifier",
                      "PhysicalOptions","Unique"]
  IDXCOL_ATTR_LIST = ["CreationDate","Creator","ModificationDate","Modifier"]
  def __init__(self):
    return

  @staticmethod
  def parse(pdmfilename):
    return xml.dom.minidom.parse(pdmfilename)

  @staticmethod
  def __get_nodes_by_path(parent,xml_path):
    curr_node = parent
    for tag in xml_path.split("/")[0:-1] :
      tag_desc = tag.split("|")
      tag_name,tag_index = tag_desc[0], ( int(tag_desc[1]) if len(tag_desc) == 2 else 0 )
      child_nodes = []
      for child_node in curr_node.childNodes :
        if child_node.nodeName == tag_name :
          child_nodes.append(child_node)
      if len(child_nodes) < tag_index + 1 :
        return []
      curr_node = child_nodes[tag_index]
    tag = xml_path.split("/")[-1]
    tag_desc = tag.split("|")
    tag_name,tag_index = tag_desc[0], ( int(tag_desc[1]) if len(tag_desc) == 2 else None )
    child_nodes = []
    for child_node in curr_node.childNodes :
      if child_node.nodeName == tag_name :
        child_nodes.append(child_node)
    if tag_index == None :
      return child_nodes
    elif len(child_nodes) < tag_index + 1 :
      return []
    else :
      curr_node = child_nodes[tag_index]
      return curr_node

  @staticmethod
  def __get_attrs_by_list(parent,attr_list):
    ret_dict = {}
    for attr in attr_list :
      ret_dict[attr] = ""
      for child in parent.childNodes :
        if child.nodeName == "a:" + attr :
          ret_dict[attr] = child.childNodes[0].data
          break
    return ret_dict

  @staticmethod
  def __get_pkgnodes_recursively(o_pkg):
    if o_pkg.nodeName != "o:Model" and o_pkg.nodeName != "o:Package" :
      return []
    ret_list = []
    subpkgs = PDMHandler.__get_nodes_by_path(o_pkg,"c:Packages/o:Package")
    if subpkgs != None :
      for subpkg in subpkgs :
        ret_list.append(subpkg)
        ret_list = ret_list + PDMHandler.__get_pkgnodes_recursively(subpkg)
    else :
      return []
    return ret_list

  @staticmethod
  def getPkgNodes(hpdm):
    ret_list = []
    try:
      o_mdl  = PDMHandler.__get_nodes_by_path(hpdm,"Model/o:RootObject/c:Children/o:Model")[0]
      ret_list.append(o_mdl)
    except IndexError:
      print ("ERROR:不是一个合法的pdm文件!")
      return []
    ret_list = ret_list + PDMHandler.__get_pkgnodes_recursively(o_mdl)
    return ret_list

  @staticmethod
  def getTblNodesInPkg(pkgnode):
    return PDMHandler.__get_nodes_by_path(pkgnode, "c:Tables/o:Table")
  @staticmethod
  def getColNodesInTbl(tblnode):
    return PDMHandler.__get_nodes_by_path(tblnode, "c:Columns/o:Column")
  @staticmethod
  def getIdxNodesInTbl(tblnode):
    return PDMHandler.__get_nodes_by_path(tblnode, "c:Indexes/o:Index")
  @staticmethod
  def getIdxColNodesInIdx(idxnode):
    return PDMHandler.__get_nodes_by_path(idxnode,"c:IndexColumns/o:IndexColumn")

  @staticmethod
  def getPkgAttrs(pkgnode):
    return PDMHandler.__get_attrs_by_list(pkgnode,PDMHandler.PKG_ATTR_LIST)
  @staticmethod
  def getTblAttrs(tblnode):
    return PDMHandler.__get_attrs_by_list(tblnode,PDMHandler.TBL_ATTR_LIST)
  @staticmethod
  def getColAttrs(colnode):
    return PDMHandler.__get_attrs_by_list(colnode,PDMHandler.COL_ATTR_LIST)
  @staticmethod
  def getIdxAttrs(idxnode):
    return PDMHandler.__get_attrs_by_list(idxnode,PDMHandler.IDX_ATTR_LIST)
  @staticmethod
  def getIdxColAttrs(idxcolnode):
    ret_dict = PDMHandler.__get_attrs_by_list(idxcolnode,PDMHandler.IDXCOL_ATTR_LIST)
    refcol   = PDMHandler.__get_nodes_by_path(idxcolnode,"c:Column/o:Column")
    try:
      refcolid = refcol[0].getAttribute("Ref")
    except IndexError :
      ret_dict["RefColCode"] = ""
      return ret_dict
    currnode = idxcolnode
    while(1) :
      currnode = currnode.parentNode
      if currnode.tagName == "o:Table" or currnode == None :
        break
    if currnode == None :
      return []
    else :
      for col in PDMHandler.getColNodesInTbl(currnode) :
        if col.getAttribute("Id") == refcolid :
          ret_dict["RefColCode"] = PDMHandler.getColAttrs(col)["Code"]
    return ret_dict


  @staticmethod
  def getNodePath(node) :
    curr = node
    path_nodes = []
    while(1):
      if curr != None and curr.nodeName != "#document" :
        path_nodes.append(curr.tagName)
      else :
        break
      curr = curr.parentNode 
    path_nodes.reverse()
    path = "".join([ slash + node for slash in '/' for node in path_nodes ])
    return path

