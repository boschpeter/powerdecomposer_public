
````
from xml.dom.minidom import parse
import xml.dom.minidom


#!/usr/bin/python
# -*- coding: utf-8 -*- #

from PDMHandler import PDMHandler
````

````
import argparse
import os.path
# import readline
import sys

from .command_executor import CommandExecutor
from .parser import PDMParser


import dataclasses
import re
from typing import List, Optional
from xml.etree import ElementTree
from xml.etree.ElementTree import Element, SubElement

from .models import TypeUtil, DataType, Column, Key, Index, Table, Sequence, Schema

namespaces = {
    'a': 'attribute',
    'c': 'collection',
    'o': 'object',
}

````