from LDMHandler import LDMHandler
import sys
import pandas as pd


def parse_rel(isa):
  print("======Relationships======")
  filename = isa
  ph = LDMHandler.parse(filename)

  for pkg in LDMHandler.getPkgNodes(ph):
    pkg_attrs = LDMHandler.getPkgAttrs(pkg)

    for rel in LDMHandler.getRelNodesInPkg(pkg):
      rel_attrs = LDMHandler.getRelAttrs(rel)
      print(" R:", rel_attrs["Name"], rel_attrs["Code"], rel_attrs["Creator"])
  print("======dze end of Relationships ======")


def parse_isa(isa):
  print("======Entities======")
  filename = isa
  ph = LDMHandler.parse(filename)
  df = pd.DataFrame()
  for pkg in LDMHandler.getPkgNodes(ph):
    pkg_attrs = LDMHandler.getPkgAttrs(pkg)

  for ent in LDMHandler.getEntNodesInPkg(pkg):
      ent_attrs = LDMHandler.getEntAttrs(ent)
      print(" E:", ent_attrs["ObjectID"], ent_attrs["Name"], ent_attrs["Code"], ent_attrs["Creator"])
      dict = {'ObjectId': ent_attrs["ObjectID"], 'Name': ent_attrs["Name"] , 'Code': ent_attrs["Code"]}


      # Create a Pandas dataframe from some data.
      df = df.append(dict, ignore_index = True)
      # Create a Pandas Excel writer using XlsxWriter as the engine.
      writer = pd.ExcelWriter('pandas_entity2.xlsx', engine='xlsxwriter')

      # Write the dataframe data to XlsxWriter. Turn off the default header and
      # index and skip one row to allow us to insert a user defined header.
      df.to_excel(writer, sheet_name='Sheet1', startrow=1, header=False, index=False)

      # Get the xlsxwriter workbook and worksheet objects.
      workbook = writer.book
      worksheet = writer.sheets['Sheet1']
      # Get the dimensions of the dataframe.
      (max_row, max_col) = df.shape
      # Create a list of column headers, to use in add_table().
      column_settings = [{'header': column} for column in df.columns]
      # Add the Excel table structure. Pandas will add the data.
      worksheet.add_table(0, 0, max_row, max_col - 1, {'columns': column_settings})
     # Make the columns wider for clarity.
      worksheet.set_column(0, max_col - 1, 12)
     # Close the Pandas Excel writer and output the Excel file.
      writer.save()


    #  for atb in LDMHandler.getAtbNodesInEnt(ent):
    #    atb_attrs = LDMHandler.getAtbAttrs(atb)
    #    print("  A:", atb_attrs["Name"], atb_attrs["Code"], atb_attrs["DataType"], atb_attrs["Length"])
  print("======dze end of entities ======")



def main(isa):
  isa = sys.argv[1]
  parse_isa(isa)

if __name__ == "__main__":
  main(sys.argv[1])  # ldm.xml
     
   
